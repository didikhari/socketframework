package iso8583.socketServer.processor;

import common.logger.Log;
import iso8583.common.transaction.ISOTransactionClient;
import java.io.IOException;

import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.iso.ISOUtil;

public class ISOSocketClientAsync implements ISORequestListener, Configurable {
	private String localIP = "localhost";
	private String sourceIP;
	private int sourcePort;
	private int localPort;
	private static Log log = Log.getInstance(ISOSocketClientAsync.class);

	protected String receiveAddress() {
		return "[" + this.localIP + ":" + this.localPort + "/" + this.sourceIP + ":" + this.sourcePort + "]";
	}

	public boolean process(ISOSource isoSrc, ISOMsg isoRequest) {
		try {
			log.debug("[SOCKET CLIENT] [Receive]" + receiveAddress() + " HEX STRING ["
					+ ISOUtil.hexString(isoRequest.pack()) + "]");

			ISOTransactionClient isoTransactionClient = new ISOTransactionClient(isoSrc, isoRequest);
			isoTransactionClient.setReceiveAddress(this.localIP, this.localPort, this.sourceIP, this.sourcePort);
			ISOMsg isoResponse = isoTransactionClient.processsingMessage();
			if (validateResponse(isoRequest, isoResponse)) {
				isoSrc.send(isoResponse);
			}
		} catch (IOException | ISOException e) {
			log.error("[SOCKET CLIENT] " + receiveAddress() + "sendISOResponse " + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	private boolean validateResponse(ISOMsg isoRequest, ISOMsg isoResponse) {
		try {
			if ((isoResponse == null) || (isoResponse.getMTI() == null) || ("".equals(isoResponse.getMTI()))
					|| (isoResponse.getMTI().equals(isoRequest.getMTI()))) {
				return false;
			}
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	@Override
	public void setConfiguration(Configuration cfg) throws ConfigurationException {
		this.localIP = cfg.get("localport", this.localIP);
		this.localPort = cfg.getInt("localport", 0);
		this.sourceIP = cfg.get("sourceip", "");
		this.sourcePort = cfg.getInt("sourceport", 0);
	}
}
