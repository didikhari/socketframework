package iso8583.socketServer.processor;

import iso8583.common.transaction.ISOTransactionServer;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;

public class ISOSocketServerAsync implements ISORequestListener {
	public boolean process(ISOSource isoSrc, ISOMsg isoMsg) {
		try {
			ISOTransactionServer processingRequest = new ISOTransactionServer(isoSrc, isoMsg);
			processingRequest.coreProcess();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
