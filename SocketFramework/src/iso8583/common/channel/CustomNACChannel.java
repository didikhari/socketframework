package iso8583.common.channel;

import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.net.SocketException;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.channel.NACChannel;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

/**
 * Custom NAC Channel for sending byte message including header and message
 * length
 * 
 * @author didikh
 *
 */
public class CustomNACChannel extends NACChannel {
	
	public CustomNACChannel() { 
		super();
	}
	
	public CustomNACChannel(String hostName, int portNumber, ISOPackager packager, byte[] bytes) {
		super(hostName, portNumber, packager, bytes);
		System.out.println("[CUSTOM CHANNEL] Create Cahnnel");
	}

	@Override
	public ISOMsg receive() throws IOException, ISOException {
		// TODO Auto-generated method stub
		byte[] b = null;
		byte[] header = null;
		LogEvent evt = new LogEvent(this, "receive");
		ISOMsg m = createMsg();

		m.setSource(this);

		try {
			if (!(isConnected())) {
				throw new ISOException("unconnected ISOChannel");
			}
			synchronized (this.serverInLock) {
				int len = getMessageLength();
				int hLen = getHeaderLength();

				if (len == -1) {
					if (hLen > 0) {
						header = readHeader(hLen);
					}
					b = streamReceive();
				} else if ((len > 0) && (len <= getMaxPacketLength())) {
					if (hLen > 0) {
						header = readHeader(hLen);
						len -= header.length;
					}
					b = new byte[len];
					getMessage(b, 0, len);
					getMessageTrailler();
				} else {
					throw new ISOException(
							"receive length " + len + " seems strange - maxPacketLength = " + getMaxPacketLength());
				}
			}

			System.out.println("Channel Recv : " + new String(b));

			// Override bit8
			String rawData = new String(b);

			if (selectMTI(rawData).equals("1430") && selectProCode(rawData).equals("011000")) {
				String validatedData;
				ISOMsg isoMsgCustom = null;
				try {
					isoMsgCustom = new ISOMsg();
					ISOPackager packager = getDynamicPackager(header, b);
					isoMsgCustom.setPackager(packager);

					validatedData = validateBit8(rawData, 0);
					System.out.println(validatedData);
					System.out.println(new String(validatedData.getBytes()));
					System.out.println("UNPACK : " + isoMsgCustom.unpack(validatedData.getBytes()));
				} catch (ISOException e) {
					try {
						isoMsgCustom = new ISOMsg();
						ISOPackager packager = getDynamicPackager(header, b);
						isoMsgCustom.setPackager(packager);

						validatedData = validateBit8(rawData, 1);
						System.out.println(validatedData);
						System.out.println(new String(validatedData.getBytes()));
						System.out.println("UNPACK : " + isoMsgCustom.unpack(validatedData.getBytes()));
						b = validatedData.getBytes();
					} catch (ISOException e1) {

						// e.printStackTrace();

						validatedData = validateBit8(rawData, 2);
						System.out.println(validatedData);
						System.out.println(new String(validatedData.getBytes()));
						try {
							System.out.println("UNPACK : " + isoMsgCustom.unpack(validatedData.getBytes()));
						} catch (ISOException e2) {
							// TODO Auto-generated catch block
							// e1.printStackTrace();
						}
						b = validatedData.getBytes();
					}
				}
			}

			// end

			m.setPackager(getDynamicPackager(header, b));
			m.setHeader(getDynamicHeader(header));

			if ((b.length > 0) && (!(shouldIgnore(header)))) {
				unpack(m, b);
			}

			m.setDirection(1);
			m = applyIncomingFilters(m, header, b, evt);
			m.setDirection(1);
			evt.addMessage(m);
			this.cnt[2] += 1;
			setChanged();
			notifyObservers(m);
		} catch (ISOException e) {
			evt.addMessage(e);
			if (header != null) {
				evt.addMessage("--- header ---");
				evt.addMessage(ISOUtil.hexdump(header));
			}
			if (b != null) {
				evt.addMessage("--- data ---");
			}

			throw e;
		} catch (EOFException e) {
			closeSocket();

			throw e;
		} catch (SocketException e) {
			closeSocket();
			if (this.usable)
				;
			throw e;
		} catch (InterruptedIOException e) {
			closeSocket();

			throw e;
		} catch (IOException e) {
			closeSocket();
			if (this.usable)
				;
			throw e;
		} catch (Exception e) {
			evt.addMessage(m);

			throw new ISOException("unexpected exception", e);
		} finally {
			Logger.log(evt);
		}
		return m;
	}

	private void closeSocket() throws IOException {
		Socket socket = getSocket();
		if (socket == null)
			return;
		try {
			socket.setSoLinger(true, 0);
		} catch (SocketException e) {
		}
		socket.close();
		socket = null;
	}

	public byte[] concat(byte[] a, byte[] b) {
		int aLen = a.length;
		int bLen = b.length;
		byte[] c = new byte[aLen + bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		return c;
	}

	public static String validateBit8(String data, int jmlhPotong) {

		String bit2Length = data.substring(36, 38);
		System.out.println(bit2Length);

		int bit8BeginIndex = 36 + Integer.parseInt(bit2Length) + 2 + 52;

		System.out.println(data.substring(bit8BeginIndex + 8, bit8BeginIndex + 8 + jmlhPotong));

		StringBuilder strBuilder = new StringBuilder(data);
		String result = strBuilder.replace(bit8BeginIndex + 8, bit8BeginIndex + 8 + jmlhPotong, "").toString();

		System.out.println(result);

		return result;
	}

	public static String selectMTI(String data) {
		String result = data.substring(0, 4);

		return result;
	}

	public static String selectProCode(String data) {
		String bit2Length = data.substring(36, 38);

		int bit3BeginIndex = 36 + Integer.parseInt(bit2Length) + 2;

		String result = data.substring(bit3BeginIndex, bit3BeginIndex + 6);

		return result;

	}
}
