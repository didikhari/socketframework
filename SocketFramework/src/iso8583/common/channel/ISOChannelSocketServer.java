package iso8583.common.channel;

import java.io.IOException;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ServerChannel;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.channel.BASE24Channel;
import org.jpos.iso.channel.BASE24TCPChannel;
import org.jpos.iso.channel.CSChannel;
import org.jpos.iso.channel.ChannelPool;
import org.jpos.iso.channel.GZIPChannel;
import org.jpos.iso.channel.HEXChannel;
import org.jpos.iso.channel.LogChannel;
import org.jpos.iso.channel.LoopbackChannel;
import org.jpos.iso.channel.NACChannel;
import org.jpos.iso.channel.NCCChannel;
import org.jpos.iso.channel.PADChannel;
import org.jpos.iso.channel.PostChannel;
import org.jpos.iso.channel.RawChannel;
import org.jpos.iso.channel.VAPChannel;
import org.jpos.iso.channel.X25Channel;
import org.jpos.iso.channel.XMLChannel;

import common.logger.Log;

@Deprecated
public class ISOChannelSocketServer {
	public static ISOChannelSocketServer instance;
	private static Log logger = Log.getInstance(ISOChannelSocketServer.class);

	public static ISOChannelSocketServer getInstance() {
		if (instance == null) {
			instance = new ISOChannelSocketServer();
		}
		return instance;
	}

	public ServerChannel getServerChannel(String log4jConfigFile, String hostName, int portNumber, ISOPackager packager,
			String isoHeader, String structureType) {
		Log.init(log4jConfigFile);
		if (structureType.equals("ASCII")) {
			return new ASCIIChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("ASCII_CS2")) {
			new ASCIIChannel(hostName, portNumber, packager) {
				protected void sendMessageLength(int arg0) throws IOException {
					super.sendMessageLength(arg0);
				}

				protected int getMessageLength() throws IOException, ISOException {
					return super.getMessageLength();
				}
			};
		}
		if (structureType.equals("NAC_FWD")) {
			CustomNACChannel ch = new CustomNACChannel(hostName, portNumber, packager, isoHeader.getBytes());
			return ch;
		}
		if (structureType.equals("NAC")) {
			NACChannel ch = new NACChannel(hostName, portNumber, packager, isoHeader.getBytes());
			return ch;
		}
		if (structureType.equals("NAC_CS1")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					super.sendMessageLength(len + 2);
				}

				protected int getMessageLength() throws IOException, ISOException {
					return super.getMessageLength() - 2;
				}
			};
		}
		if (structureType.equals("NAC_RINTIS_1")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					super.sendMessageLength(len + 2);
				}

				protected int getMessageLength() throws IOException, ISOException {
					return super.getMessageLength() - 2;
				}
			};
		}
		if (structureType.equals("NAC_CS2")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					ISOChannelSocketServer.logger.debug("NAC_CS2 Send len" + len);
					super.sendMessageLength(len);
				}

				protected int getMessageLength() throws IOException, ISOException {
					int messageLength = super.getMessageLength();
					ISOChannelSocketServer.logger.debug("NAC_CS2 Receive len" + messageLength);
					return messageLength;
				}

				protected int getHeaderLength() {
					int headerLength = super.getHeaderLength();
					ISOChannelSocketServer.logger.debug("NAC_CS2 Receive headerLen" + headerLength);
					return headerLength;
				}
			};
		}
		if (structureType.equals("Base24")) {
			return new BASE24Channel(hostName, portNumber, packager);
		}
		if (structureType.equals("BASE24TCP")) {
			return new BASE24TCPChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("CS")) {
			return new CSChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("ChannelPool")) {
			return (ServerChannel) new ChannelPool();
		}
		if (structureType.equals("GZIP")) {
			return new GZIPChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("HEX")) {
			return new HEXChannel(hostName, portNumber, packager, isoHeader.getBytes());
		}
		if (structureType.equals("Log")) {
			return new LogChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("Loopback")) {
			return (ServerChannel) new LoopbackChannel();
		}
		if (structureType.equals("NCC")) {
			return new NCCChannel(hostName, portNumber, packager, isoHeader.getBytes());
		}
		if (structureType.equals("PAD")) {
			return new PADChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("Post")) {
			return new PostChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("Raw")) {
			return new RawChannel(hostName, portNumber, packager, isoHeader.getBytes());
		}
		if (structureType.equals("VAP")) {
			return new VAPChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("X25")) {
			return new X25Channel(hostName, portNumber, packager);
		}
		if (structureType.equals("XML")) {
			return new XMLChannel(hostName, portNumber, packager);
		}
		logger.error("[SOCKET SERVER] Not Found Channel Server");
		return null;
	}
}
