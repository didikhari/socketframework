package iso8583.common.channel;

import java.io.IOException;
import java.net.SocketException;

import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.channel.BASE24Channel;
import org.jpos.iso.channel.BASE24TCPChannel;
import org.jpos.iso.channel.CSChannel;
import org.jpos.iso.channel.ChannelPool;
import org.jpos.iso.channel.GZIPChannel;
import org.jpos.iso.channel.HEXChannel;
import org.jpos.iso.channel.LogChannel;
import org.jpos.iso.channel.LoopbackChannel;
import org.jpos.iso.channel.NACChannel;
import org.jpos.iso.channel.NCCChannel;
import org.jpos.iso.channel.PADChannel;
import org.jpos.iso.channel.PostChannel;
import org.jpos.iso.channel.RawChannel;
import org.jpos.iso.channel.VAPChannel;
import org.jpos.iso.channel.X25Channel;
import org.jpos.iso.channel.XMLChannel;

import common.logger.Log;
import iso8583.common.util.ISOMessageUtil;

@Deprecated
public class ISOChannelSocketClient {
	public static ISOChannelSocketClient instance;
	private static Log logger = Log.getInstance(ISOChannelSocketClient.class);

	public static ISOChannelSocketClient getInstance() {
		if (instance == null) {
			instance = new ISOChannelSocketClient();
		}
		return instance;
	}

	public ISOChannel getClientChannel(String log4jConfigFile, String hostName, int portNumber, ISOPackager packager,
			String isoHeader, String structureType) {
		Log.init(log4jConfigFile);
		logger.debug("[SOCKET CLIENT] getClientChannel" + hostName + "/" + portNumber + "/" + isoHeader + "/"
				+ structureType + "/");
		if (structureType.equals("ASCII")) {
			return new ASCIIChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("ASCII_HEX2")) {
			new ASCIIChannel(hostName, portNumber, packager) {
				protected void getMessageTrailler() throws IOException {
					byte[] b = new byte[1];
					this.serverIn.readFully(b, 0, 1);
				}

				protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
					this.serverOut.write(Character.toString('\003').getBytes());
				}

				protected void sendMessageLength(int len) throws IOException {
					try {
						len++;
						StringBuffer sb = ISOMessageUtil.getByteMsgLength(new StringBuffer(), len);
						this.serverOut.write(sb.toString().getBytes());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				protected int getMessageLength() throws IOException, ISOException {
					byte[] b = new byte[2];
					this.serverIn.readFully(b, 0, 2);
					int len = (b[0] & 0xFF) << 8 | b[1] & 0xFF;
					len--;
					return len;
				}
			};
		}
		if (structureType.equals("NAC")) {
			return new NACChannel(hostName, portNumber, packager, isoHeader.getBytes());
		}
		if (structureType.equals("NAC_CS1")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					super.sendMessageLength(len + 2);
				}

				protected int getMessageLength() throws IOException, ISOException {
					return super.getMessageLength() - 2;
				}
			};
		}
		if (structureType.equals("NAC_B24")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					super.sendMessageLength(len);
				}

				protected int getMessageLength() throws IOException, ISOException {
					return super.getMessageLength() - 2;
				}

				protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
					this.serverOut.write(Character.toString('\003').getBytes());
				}
			};
		}
		if (structureType.equals("NAC_B24_2")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					super.sendMessageLength(len);
				}

				protected int getMessageLength() throws IOException, ISOException {
					return super.getMessageLength();
				}

				protected void getMessageTrailler() throws IOException {
					byte[] b = new byte[1];
					this.serverIn.readFully(b, 0, 1);
				}

				protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
					this.serverOut.write(Character.toString('\003').getBytes());
				}
			};
		}
		if (structureType.equals("NAC_CS2")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					ISOChannelSocketClient.logger.debug("NAC_CS2 Send len " + len);
					super.sendMessageLength(len);
				}

				protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
					this.serverOut.write(Character.toString('\003').getBytes());
				}

				protected int getMessageLength() throws IOException, ISOException {
					int messageLength = super.getMessageLength();
					ISOChannelSocketClient.logger.debug("NAC_CS2 Receive len" + messageLength);
					return messageLength;
				}

				protected int getHeaderLength() {
					int headerLength = super.getHeaderLength();
					ISOChannelSocketClient.logger.debug("NAC_CS2 Receive headerLen" + headerLength);
					return headerLength;
				}
			};
		}
		if (structureType.equals("NAC_RINTIS_1")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					ISOChannelSocketClient.logger.debug("NAC_RINTIS_1 Send len " + len);
					super.sendMessageLength(len + 1);
				}

				protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
					this.serverOut.write(Character.toString('\003').getBytes());
				}

				protected int getMessageLength() throws IOException, ISOException {
					int messageLength = super.getMessageLength();
					ISOChannelSocketClient.logger.debug("NAC_RINTIS_1 Receive len" + messageLength);
					return messageLength;
				}

				protected int getHeaderLength() {
					int headerLength = super.getHeaderLength();
					ISOChannelSocketClient.logger.debug("NAC_RINTIS_1 Receive headerLen" + headerLength);
					return headerLength;
				}
			};
		}
		if (structureType.equals("NAC_RINTIS_2")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					ISOChannelSocketClient.logger.debug("NAC_RINTIS_1 Send len " + len);
					super.sendMessageLength(len + 1);
				}

				protected int getMessageLength() throws IOException, ISOException {
					int messageLength = super.getMessageLength();
					ISOChannelSocketClient.logger.debug("NAC_RINTIS_1 Receive len" + messageLength);
					return messageLength;
				}

				protected int getHeaderLength() {
					int headerLength = super.getHeaderLength();
					ISOChannelSocketClient.logger.debug("NAC_RINTIS_1 Receive headerLen" + headerLength);
					return headerLength;
				}
			};
		}
		if (structureType.equals("NAC_CS3")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				

				private void closeSocket() throws IOException {
					if (getSocket() != null) {
						try {
							getSocket().setSoLinger(true, 0);
						} catch (SocketException localSocketException) {
						}
						getSocket().close();
					}
				}
			};
		}
		if (structureType.equals("NAC_CS4")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					ISOChannelSocketClient.logger.debug("NAC_CS4 Send len" + len);
					len++;
					super.sendMessageLength(len);
				}

				protected int getMessageLength() throws IOException, ISOException {
					int len = super.getMessageLength();
					len--;
					ISOChannelSocketClient.logger.debug("NAC_CS4 Receive len" + len);
					return len;
				}

				protected int getHeaderLength() {
					int headerLength = super.getHeaderLength();
					ISOChannelSocketClient.logger.debug("NAC_CS4 Receive headerLen" + headerLength);
					return headerLength;
				}

				protected void getMessageTrailler() throws IOException {
					byte[] b = new byte[1];
					this.serverIn.readFully(b, 0, 1);
				}

				protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
					this.serverOut.write(Character.toString('\003').getBytes());
				}
			};
		}
		if (structureType.equals("NAC_CS5")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					super.sendMessageLength(len + 2);
				}

				protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
					this.serverOut.write(Character.toString('\003').getBytes());
				}

				protected int getMessageLength() throws IOException, ISOException {
					return super.getMessageLength() - 2;
				}
			};
		}
		if (structureType.equals("NAC_CS6")) {
			new NACChannel(hostName, portNumber, packager, isoHeader.getBytes()) {
				protected void sendMessageLength(int len) throws IOException {
					super.sendMessageLength(len + 3);
				}

				protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
					this.serverOut.write(Character.toString('\003').getBytes());
				}

				protected int getMessageLength() throws IOException, ISOException {
					return super.getMessageLength() - 3;
				}
			};
		}
		if (structureType.equals("Base24")) {
			return new BASE24Channel(hostName, portNumber, packager);
		}
		if (structureType.equals("BASE24TCP")) {
			return new BASE24TCPChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("CS")) {
			return new CSChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("ChannelPool")) {
			return new ChannelPool();
		}
		if (structureType.equals("GZIP")) {
			return new GZIPChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("HEX")) {
			return new HEXChannel(hostName, portNumber, packager, isoHeader.getBytes());
		}
		if (structureType.equals("Log")) {
			return new LogChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("Loopback")) {
			return new LoopbackChannel();
		}
		if (structureType.equals("NCC")) {
			return new NCCChannel(hostName, portNumber, packager, isoHeader.getBytes());
		}
		if (structureType.equals("PAD")) {
			return new PADChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("Post")) {
			return new PostChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("Raw")) {
			return new RawChannel(hostName, portNumber, packager, isoHeader.getBytes());
		}
		if (structureType.equals("VAP")) {
			return new VAPChannel(hostName, portNumber, packager);
		}
		if (structureType.equals("X25")) {
			return new X25Channel(hostName, portNumber, packager);
		}
		if (structureType.equals("XML")) {
			return new XMLChannel(hostName, portNumber, packager);
		}
		logger.error("[SOCKET CLIENT] Not Found Client Channel Type");
		return null;
	}
}
