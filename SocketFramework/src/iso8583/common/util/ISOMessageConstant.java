package iso8583.common.util;

public final class ISOMessageConstant {
	public static final String ISO_NETMNT_MTI = "0800";
	public static final String ISO_NETMNT_MTI_REPLY = "0810";
}
