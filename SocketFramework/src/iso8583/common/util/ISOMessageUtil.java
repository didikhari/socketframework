package iso8583.common.util;

import org.jpos.iso.ISOUtil;

public class ISOMessageUtil {
	public static StringBuffer getByteMsgLength(StringBuffer sb, int len) {
		String hexStr = Integer.toHexString(len);
		String[] hexString_ = null;
		int lengthHex2String = hexStr.length();
		int mod = lengthHex2String % 2;
		if (lengthHex2String < 4) {
			while ((hexStr = "0" + hexStr).length() != 4) {
			}
		} else if (mod != 0) {
			hexStr = "0" + hexStr;
		}
		hexString_ = new String[hexStr.length() / 2];
		byte[] bytes = new byte[hexStr.length() / 2];
		int x = 0;
		while (x < hexString_.length) {
			hexString_[x] = hexStr.substring(x * 2, (x + 1) * 2);
			bytes = ISOUtil.hex2byte(hexString_[x]);
			sb.append(new String(bytes));
			x++;
		}
		return sb;
	}

	public static String intToASCIIString(int number) {
		StringBuilder ascii = new StringBuilder();

		int divRes = number / 255;
		int modRes = number % 255;

		ascii.append((char) divRes);
		ascii.append((char) modRes);
		return ascii.toString();
	}

	public static String convertHexToString(String hex) {
		StringBuilder sb = new StringBuilder();
		StringBuilder temp = new StringBuilder();
		for (int i = 0; i < hex.length() - 1; i += 2) {
			String output = hex.substring(i, i + 2);
			int decimal = Integer.parseInt(output, 16);
			sb.append((char) decimal);
			temp.append(decimal);
		}
		return sb.toString();
	}

	/*
	 * public static int asciiStringToInt(String ascii) { char[] arrChar =
	 * ascii.toCharArray(); return arrChar[0] * '��' + arrChar[1]; }
	 */
}
