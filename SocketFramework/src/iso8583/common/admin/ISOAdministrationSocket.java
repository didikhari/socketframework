package iso8583.common.admin;

import org.jpos.iso.ISOUtil;
import org.jpos.q2.Q2;

public class ISOAdministrationSocket 
{
	private Q2 q2;
	
	private static final ISOAdministrationSocket INSTANCE = new ISOAdministrationSocket();
	private ISOAdministrationSocket() {
		//Log.init(log4jConfigFile);
	}
	
	public static ISOAdministrationSocket getInstance()
	{
		return INSTANCE;
	}
	
	public void start(String deployDir) 
	{
		if(q2 == null)
			q2 = new Q2(deployDir);
		
		if(!q2.running())
		{
			q2.start();
		}
		else
		{
			System.out.println("Q2 is Already running");
		}
	}
	
	/**
	 * shutdown and wait current q2 instance <i>(if any)</i>, then start with new q2 instance
	 * @param deployDir
	 */
	public void restart(String deployDir) 
	{
		if(q2 != null && q2.running())
		{
			q2.shutdown();
			ISOUtil.sleep(10000l);
		}
		q2 = new Q2(deployDir);
		q2.start();
	}
	
	public void stop() 
	{
		if(q2 != null && q2.running())
		{
			q2.shutdown();
			q2 = null;
		}
	}
}
