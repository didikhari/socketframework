/*package iso8583.common.admin;

import java.util.ArrayList;
import java.util.List;

import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMUX;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;

import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataUtil;

import common.logger.Log;
import iso8583.common.cache.ISOTransactionCache;
import iso8583.common.channel.ISOChannelSocketClient;
import iso8583.socketServer.processor.ISOSocketClientAsync;

public class ISOAdministrationSocketClientSync
{
  String localIP = "localhost";
  private static Log log = Log.getInstance(ISOAdministrationSocketClientSync.class);
  
  public void startingSocketClient(IData pipeline)
    throws Exception
  {
    processSocketClient(pipeline);
  }
  
  public void stoppingSocketClient(String Backend, String log4jConfigFile)
  {
    stopSocketCLient(Backend, log4jConfigFile);
  }
  
  public String checkingSocketClient(String Backend, String log4jConfigFile)
  {
    return checkSocketClient(Backend, log4jConfigFile);
  }
  
  public void processSocketClient(IData pipeline)
    throws Exception
  {
    IDataCursor pipelineCursor = pipeline.getCursor();
    
    IData socketOutBoundConfig = IDataUtil.getIData(pipelineCursor, "socketOutBoundConfig");
    
    IDataCursor socketOutBoundConfigCursor = socketOutBoundConfig.getCursor();
    String Backend = IDataUtil.getString(socketOutBoundConfigCursor, "Backend");
    String DestinationIP = IDataUtil.getString(socketOutBoundConfigCursor, "DestinationIP");
    String DestinationPort = IDataUtil.getString(socketOutBoundConfigCursor, "DestinationPort");
    String IsoHeader = IDataUtil.getString(socketOutBoundConfigCursor, "IsoHeader");
    String PackagerPath = IDataUtil.getString(socketOutBoundConfigCursor, "PackagerPath");
    String StructureType = IDataUtil.getString(socketOutBoundConfigCursor, "StructureType");
    String InvokeService = IDataUtil.getString(socketOutBoundConfigCursor, "InvokeService");
    String SourcePort = IDataUtil.getString(socketOutBoundConfigCursor, "SourcePort");
    String ChannelIP = IDataUtil.getString(socketOutBoundConfigCursor, "ChannelIP");
    String ChannelPort = IDataUtil.getString(socketOutBoundConfigCursor, "ChannelPort");
    socketOutBoundConfigCursor.destroy();
    IData log4jConfiguration = IDataUtil.getIData(pipelineCursor, "log4jConfiguration");
    if (log4jConfiguration == null)
    {
      log.error("ISOAdministrationSocketClientSync.startingSocketClient: log4jConfiguration is null");
      throw new Exception("log4jConfiguration is null!");
    }
    IDataCursor log4jConfigurationCursor = log4jConfiguration.getCursor();
    String log4jConfigFile = IDataUtil.getString(log4jConfigurationCursor, "log4jConfigFile");
    log4jConfigurationCursor.destroy();
    pipelineCursor.destroy();
    Log.init(log4jConfigFile);
    
    log.info("[SOCKET CLIENT] [Starting...] Socket Client");
    startSocketClient(log4jConfigFile, DestinationIP, Integer.parseInt(DestinationPort), 
    		PackagerPath, IsoHeader, Backend, StructureType, SourcePort, ChannelIP, Integer.parseInt(ChannelPort));
    log.info("CACHE NAME = " + DestinationIP + ":" + DestinationPort + " Invoke Service =" + InvokeService);
    ISOTransactionCache.serviceClient_ns.put(DestinationIP + ":" + DestinationPort, InvokeService);
    
    log.info("Already Cached = " + (String)ISOTransactionCache.serviceClient_ns.get(new StringBuilder(String.valueOf(DestinationIP)).append(":").append(DestinationPort).toString()));
  }
  
  protected void startSocketClient(String log4jConfigFile, String hostName, int portNumber, 
		  String packagerPath, String isoHeader, String connectionID, String structureType,
		  String SourcePort, String fwIp, int fwPort)
  {
    Log.init(log4jConfigFile);
    log.info("[SOCKET CLIENT] [Start] " + hostName + ":" + portNumber + ";" + connectionID + "-" + isoHeader + ";" + structureType + ";" + packagerPath);
    try
    {
      ISOPackager packager = new GenericPackager(packagerPath);
      ISOChannel channel = ISOChannelSocketClient.getInstance().getClientChannel(log4jConfigFile, hostName, portNumber, packager, isoHeader, structureType);
      //channel.setName(connectionID);

	  Logger logger = new Logger();
      //logger.addListener(new ISOExLogListener(SourcePort, fwIp, fwPort));
      logger.addListener(new SimpleLogListener(System.out));
      if(channel instanceof BaseChannel)
      {
    	  ((BaseChannel)channel).setLogger(logger, connectionID+"-logger");
      }
      ISOMUX isoMux = new ISOMUX(channel)
      {
        protected String getKey(ISOMsg m)
          throws ISOException
        {
          return super.getKey(m);
        }
      };
      isoMux.setISORequestListener(new ISOSocketClientAsync(this.localIP, 
        0, 
        hostName, 
        portNumber));
      isoMux.setLogger(logger, connectionID+"-mux-logger");
      new Thread(isoMux).start();
      ISOTransactionCache.isoMux_obj.put(connectionID, isoMux);
    }
    catch (ISOException e)
    {
      log.error("[SOCKET CLIENT] " + e.getMessage());
      e.printStackTrace();
    }
    log.info("[SOCKET CLIENT] [Finish] " + connectionID);
  }
  
  protected void stopSocketCLient(String Backend, String log4jConfigFile)
  {
    Log.init(log4jConfigFile);
    log.info("[SOCKET CLIENT] [Stopping...] " + Backend);
    ISOMUX isomux = (ISOMUX)ISOTransactionCache.isoMux_obj.get(Backend);
    if (isomux != null)
    {
      new Thread(new stopRunnable(isomux, Backend)).start();
      try
      {
        Thread.sleep(5000L);
      }
      catch (InterruptedException e)
      {
        e.printStackTrace();
      }
    }
    else
    {
      log.info("[SOCKET CLIENT] Not Found Connection " + Backend);
    }
  }
  
  protected String checkSocketClient(String Backend, String log4jConfigFile)
  {
    Log.init(log4jConfigFile);
    String status = "Not Connected";
    log.info("[SOCKET CLIENT] [Checking...] " + Backend);
    ISOMUX isomux = (ISOMUX)ISOTransactionCache.isoMux_obj.get(Backend);
    if (isomux != null)
    {
      if (isomux.isConnected()) {
        status = "Connected";
      }
      log.info("[SOCKET CLIENT] isConnected?" + isomux.isConnected());
    }
    else
    {
      status = "Not Found Connection";
      log.info("[SOCKET CLIENT] Not Found Connection " + Backend);
    }
    return status;
  }
  
  private class stopRunnable
    implements Runnable
  {
    ISOMUX isomux;
    String backend;
    
    public stopRunnable(ISOMUX isomux, String backend)
    {
      this.isomux = isomux;
      this.backend = backend;
    }
    
    public void run()
    {
      this.isomux.terminate();
      
      ISOTransactionCache.isoMux_obj.remove(this.backend);
    }
  }
  
  public List<String> getListCacheServiceClient()
  {
    List<String> caches = new ArrayList<String>();
    for (int i = 0; i < ISOTransactionCache.serviceClient_ns.size(); i++) {
      caches.add((String)ISOTransactionCache.serviceClient_ns.get(Integer.valueOf(i)));
    }
    return caches;
  }
  
  public List<String> getListCacheServiceServer()
  {
    List<String> caches = new ArrayList<String>();
    for (int i = 0; i < ISOTransactionCache.service_ns.size(); i++) {
      caches.add((String)ISOTransactionCache.service_ns.get(Integer.valueOf(i)));
    }
    return caches;
  }
}
*/