/*package iso8583.common.admin;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ServerChannel;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.util.ThreadPool;

import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataUtil;

import common.logger.Log;
import iso8583.common.cache.ISOTransactionCache;
import iso8583.common.channel.ISOChannelSocketServer;
import iso8583.socketServer.processor.ISOSocketServerAsync;

public class ISOAdministrationSocketServerSync {
	private static Log logger = Log.getInstance(ISOAdministrationSocketServerSync.class);

	public void startingSocketServer(IData pipeline) throws Exception {
		processSocketServerConnection(pipeline);
	}

	public String stoppingSocketServer(String portNumber, String log4jConfigFile) {
		return stopSocketServer(portNumber, log4jConfigFile);
	}

	protected void processSocketServerConnection(IData pipeline) throws Exception {
		IDataCursor pipelineCursor = pipeline.getCursor();

		IData socketInBoundConfig = IDataUtil.getIData(pipelineCursor, "socketInBoundConfig");
		IDataCursor socketInBoundConfigCursor = socketInBoundConfig.getCursor();
		String ServerName = IDataUtil.getString(socketInBoundConfigCursor, "ServerName");
		String ListenIP = IDataUtil.getString(socketInBoundConfigCursor, "ListenIP");
		String ListenPort = IDataUtil.getString(socketInBoundConfigCursor, "ListenPort");
		String InvokeService = IDataUtil.getString(socketInBoundConfigCursor, "InvokeService");
		String ProcessorPoolMinSize = IDataUtil.getString(socketInBoundConfigCursor, "ProcessorPoolMinSize");
		String ProcessorPoolMaxSize = IDataUtil.getString(socketInBoundConfigCursor, "ProcessorPoolMaxSize");
		String IsoHeader = IDataUtil.getString(socketInBoundConfigCursor, "IsoHeader");
		String PackagerPath = IDataUtil.getString(socketInBoundConfigCursor, "PackagerPath");
		String StructureType = IDataUtil.getString(socketInBoundConfigCursor, "StructureType");
		socketInBoundConfigCursor.destroy();

		IData log4jConfiguration = IDataUtil.getIData(pipelineCursor, "log4jConfiguration");
		if (log4jConfiguration == null) {
			logger.error("ISOAdministrationSocketServerSync.startingSocketServer: log4jConfiguration is null");
			throw new Exception("log4jConfiguration is null!");
		}
		IDataCursor log4jConfigurationCursor = log4jConfiguration.getCursor();
		String log4jConfigFile = IDataUtil.getString(log4jConfigurationCursor, "log4jConfigFile");
		log4jConfigurationCursor.destroy();

		pipelineCursor.destroy();
		Log.init(log4jConfigFile);

		logger.info("[SOCKET SERVER] [Starting...]  " + ServerName + " " + ListenIP + ":" + ListenPort);

		startSocketServer(log4jConfigFile, ListenIP, Integer.parseInt(ListenPort), PackagerPath, IsoHeader,
				Integer.parseInt(ProcessorPoolMinSize), Integer.parseInt(ProcessorPoolMaxSize), StructureType, ServerName);
		ISOTransactionCache.service_ns.put(ListenPort, InvokeService);
	}

	protected void startSocketServer(String log4jConfigFile, String hostName, int portNumber, String packagerPath,
			String isoHeader, int minThreadPool, int maxThreadPool, String structureType, String serverName) {
		Log.init(log4jConfigFile);
		logger.info("[SOCKET SERVER] [Start] " + hostName + ":" + portNumber + ";" + minThreadPool + "-" + maxThreadPool
				+ ";" + structureType + ";" + packagerPath);
		
		try {
			ThreadPool pool = new ThreadPool(minThreadPool, maxThreadPool);
			ISOPackager packager = new GenericPackager(packagerPath);
			ServerChannel channel = ISOChannelSocketServer.getInstance().getServerChannel(log4jConfigFile, hostName,
					portNumber, packager, isoHeader, structureType);
			
			ISOServer server = new ISOServer(portNumber, channel, pool);
			//server.setName(serverName);
			server.addISORequestListener(new ISOSocketServerAsync());

			Thread thread = new Thread(server);
			thread.start();

			ISOTransactionCache.isoServer_obj.put(Integer.toString(portNumber) + "Server", server);
		} catch (ISOException e) {
			e.printStackTrace();
			logger.error("[SOCKET SERVER] " + e.getMessage());
		}
		logger.info("[SOCKET SERVER] [Finish] " + Integer.toString(portNumber) + "Server");
	}

	protected String stopSocketServer(String portNumber, String log4jConfigFile) {
		Log.init(log4jConfigFile);
		logger.info("[SOCKET SERVER] [Stopping...] " + portNumber + "Server");

		ISOServer server = (ISOServer) ISOTransactionCache.isoServer_obj.get(portNumber + "Server");
		if (server == null) {
			logger.info("[SOCKET SERVER] Server not found");

			return "Not found socket server";
		}
		server.shutdown();
		server = null;

		ISOTransactionCache.isoServer_obj.remove(portNumber + "Server");
		ISOTransactionCache.service_ns.remove(portNumber);

		logger.info("[SOCKET SERVER] Server shutdown");

		return "Success shutdown socket server";
	}
}
*/