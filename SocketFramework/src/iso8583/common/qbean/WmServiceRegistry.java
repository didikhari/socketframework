package iso8583.common.qbean;

import java.util.List;

import org.jdom.Element;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.NameRegistrar;
import org.jpos.util.NameRegistrar.NotFoundException;

@SuppressWarnings("unchecked")
public class WmServiceRegistry extends QBeanSupport implements WmServiceRegistryMBean{

	private static final String prefix = "wmservice.";
	
	@Override
	protected void startService() throws Exception {
		super.startService();
		List<Element> services = getPersist().getChildren("wm-service");
		for (Element service : services) 
		{
			String name = service.getAttributeValue("name");
			String value = service.getAttributeValue("value");
			NameRegistrar.register(prefix+name, value);
		}
	}
	
	@Override
	protected void stopService() throws Exception {
		super.stopService();
		List<Element> services = getPersist().getChildren("wm-service");
		for (Element service : services) 
		{
			String name = service.getAttributeValue("name");
			NameRegistrar.unregister(prefix+name);
		}
	}
	
	public static String getWmService(String name) throws NotFoundException
	{
		return (String) NameRegistrar.get(prefix+name);
	}
}
