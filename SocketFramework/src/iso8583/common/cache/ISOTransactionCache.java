package iso8583.common.cache;

import java.util.concurrent.ConcurrentHashMap;

import org.jpos.iso.ISOMUX;
import org.jpos.iso.ISOServer;

@Deprecated
public class ISOTransactionCache {
	public static ConcurrentHashMap<String, String> service_ns = new ConcurrentHashMap<String, String>();
	public static ConcurrentHashMap<String, String> serviceClient_ns = new ConcurrentHashMap<String, String>();
	public static ConcurrentHashMap<String, ISOServer> isoServer_obj = new ConcurrentHashMap<String, ISOServer>();
	public static ConcurrentHashMap<String, ISOMUX> isoMux_obj = new ConcurrentHashMap<String, ISOMUX>();
}
