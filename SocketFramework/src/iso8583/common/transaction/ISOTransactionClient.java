package iso8583.common.transaction;

import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.MUX;
import org.jpos.q2.iso.QMUX;
import org.jpos.util.NameRegistrar.NotFoundException;

import com.wm.app.b2b.server.InvokeState;
import com.wm.app.b2b.server.Service;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataFactory;
import com.wm.data.IDataUtil;

import common.logger.Log;
import iso8583.common.qbean.WmServiceRegistry;

public class ISOTransactionClient {
	private String localIP = "localhost";
	private String sourceIP;
	private int sourcePort;
	private int localPort;
	private ISOSource isoSrc;
	private ISOMsg isoMsg;
	private static Log log = Log.getInstance(ISOTransactionClient.class);
	private String Backend;
	private String DestinationIP;
	private String DestinationPort;
	private String TimeOut;
	private String reference1;
	private String reference3;

	public ISOTransactionClient() {
	}

	public ISOTransactionClient(ISOSource isoSrc, ISOMsg isoMsg) {
		this.isoSrc = isoSrc;
		this.isoMsg = isoMsg;
	}

	public void setReceiveAddress(String localIP, int localPort, String sourceIP, int sourcePort) {
		this.localIP = localIP;
		this.localPort = localPort;
		this.sourceIP = sourceIP;
		this.sourcePort = sourcePort;
	}

	protected String sendAddress(String destinationIP, String destinationPort, String timeout) {
		return "[" + destinationIP + ":" + destinationPort + "/" + timeout + "]";
	}

	protected String receiveAddress() {
		return "[" + this.localIP + ":" + this.localPort + "/" + this.sourceIP + ":" + this.sourcePort + "]";
	}

	protected String isoMessageAsString(ISOMsg isoMsg) {
		String isoMessage = null;
		try {
			isoMessage = new String(isoMsg.pack());
		} catch (NullPointerException | ISOException e) {
			log.error("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
					+ "isoMessageAsString " + e.getMessage());
			e.printStackTrace();
		}
		return isoMessage;
	}

	protected String isoMessageAsHexString(ISOMsg isoMsg) {
		String isoMessage = null;
		try {
			isoMessage = ISOUtil.hexString(isoMsg.pack());
		} catch (NullPointerException | ISOException e) {
			log.error("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
					+ "isoMessageAsHexString " + e.getMessage());
			e.printStackTrace();
		}
		return isoMessage;
	}

	protected MUX getConnection(String connectionID) throws NotFoundException {
		return QMUX.getMUX(connectionID);// (ISOMUX) ISOTransactionCache.isoMux_obj.get(connectionID);
	}

	protected String setISOHeader(ISOMsg isoMsg) {
		if (isoMsg.getHeader() != null) {
			return new String(isoMsg.getHeader());
		}
		return null;
	}

	protected ISOMsg errorHandler(ISOMsg isoMsg, int bitInfo, String responseMessage, String errMessage) {
		try {
			log.error("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
					+ "[Send]" + errMessage);

			isoMsg.set(bitInfo, responseMessage);
		} catch (ISOException e) {
			log.warn("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut()) + "[Send]"
					+ e.getStackTrace());
			e.printStackTrace();
		}
		return isoMsg;
	}

	public void coreProcess(IData pipeline) 
	{
		try {
			IDataCursor pipelineCursor = pipeline.getCursor();
			connectionConfig(pipelineCursor);

			ISOMsg isoMsgResponse = new ISOMsg();
			ISOMsg isoMsgRequest = new ISOMsg();
			isoMsgRequest.setPackager(isoMsgRequest.getPackager());

			MUX isomux = getConnection(getBackend());
			if (isomux == null) {
				try {
					isoMsgRequest = constructISORequest(pipelineCursor, isoMsgRequest);
					isoMsgResponse = isoMsgRequest;
					isoMsgResponse = errorHandler(isoMsgResponse, 127, "EAINF",
							"Connection " + getBackend() + " Not Found");
				} catch (ISOException e) {
					isoMsgResponse = isoMsgRequest;
					isoMsgResponse = errorHandler(isoMsgResponse, 127, "EAIERR", e.getStackTrace().toString());
					log.error("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
							+ "Send/Receive Request Connected " + e.getMessage());
					e.printStackTrace();
				}
			} else {
				ISOChannel channel = BaseChannel.getChannel(getBackend()+"-adaptor");
				isoMsgRequest.setPackager(channel.getPackager());
				if (isomux.isConnected()) {
					try {
						isoMsgRequest = constructISORequest(pipelineCursor, isoMsgRequest);
						log.info("[SOCKET CLIENT] [Send]"
								+ sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut()) + "["
								+ isoMessageAsString(isoMsgRequest) + "]");

						isoMsgResponse = isomux.request(isoMsgRequest, Long.parseLong(getTimeOut()));
						log.debug("[SOCKET CLIENT] [Send]"
								+ sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut()) + " HEX STRING ["
								+ isoMessageAsHexString(isoMsgRequest) + "]");
						if (isoMsgResponse == null) {
							isoMsgResponse = isoMsgRequest;
							isoMsgResponse = errorHandler(isoMsgResponse, 127, "EAITO",
									"Request Timeout from " + getBackend());
						} else {
							log.info("[SOCKET CLIENT] [Receive]"
									+ sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut()) + "["
									+ isoMessageAsString(isoMsgRequest) + "]");
						}
					} catch (ISOException e) {
						isoMsgResponse = isoMsgRequest;
						isoMsgResponse = errorHandler(isoMsgResponse, 127, "EAIERR", e.getStackTrace().toString());
						log.error("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
								+ "Send/Receive Request Connected " + e.getMessage());
						e.printStackTrace();
					}
				} else {
					try {
						isoMsgRequest = constructISORequest(pipelineCursor, isoMsgRequest);
						isoMsgResponse = isoMsgRequest;
						isoMsgResponse = errorHandler(isoMsgResponse, 127, "EAINC",
								"Connection " + getBackend() + " Not Connected");
						log.error("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
								+ "EAINC ");
					} catch (ISOException e) {
						isoMsgResponse = isoMsgRequest;
						isoMsgResponse = errorHandler(isoMsgResponse, 127, "EAIERR", e.getStackTrace().toString());
						log.error("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
								+ "constructISORequest EAINC " + e.getMessage());
						e.printStackTrace();
					}
				}
			}
			pipelineCursor.destroy();

			IDataCursor pipelineCursor_1 = pipeline.getCursor();
			try {
				constructISOResponse(pipelineCursor_1, isoMsgResponse);
			} catch (ISOException e) {
				isoMsgResponse = isoMsgRequest;
				isoMsgResponse = errorHandler(isoMsgResponse, 127, "EAICMS", e.getStackTrace().toString());
				log.error("[SOCKET CLIENT] " + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
						+ "constructISOResponse " + e.getMessage());
				e.printStackTrace();
			}
			log.debug("[SOCKET CLIENT] [Receive]" + sendAddress(getDestinationIP(), getDestinationPort(), getTimeOut())
					+ " HEX STRING [" + isoMessageAsHexString(isoMsgResponse) + "]");

			pipelineCursor_1.destroy();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}

	public ISOMsg processsingMessage() {
		ISOMsg isoMsgProcess = new ISOMsg();
		isoMsgProcess.setPackager(this.isoMsg.getPackager());
		try {
			log.debug("[SOCKET CLIENT] [Receive]" + receiveAddress() + " HEX STRING ["
					+ isoMessageAsHexString(this.isoMsg) + "]");
			log.info("[SOCKET CLIENT] [Receive]" + receiveAddress() + " [" + isoMessageAsString(this.isoMsg) + "]");
			log.info("IP = " + this.sourceIP + " , PORT " + this.sourcePort);
			String invokeService = "";
			try {
				invokeService = WmServiceRegistry.getWmService(this.sourceIP + ":" + this.sourcePort);
			} catch (Exception e) {
				log.info("IN EXCEPTION = YES reason " + e.getMessage());
				invokeService = "eai.bdi.biller.billerServices.Rintis.service:receiveISO8583Message";
			}
			int separator = invokeService.indexOf(":");
			if ((!"".equals(invokeService)) && (invokeService != null)) {
				IData input = mappingDataRequest(this.isoSrc, this.isoMsg);

				InvokeState state = new InvokeState();
				state.setCheckAccess(false);
				InvokeState.setCurrentState(state);
				IData output = Service.doInvoke(invokeService.substring(0, separator),
						invokeService.substring(separator + 1), input);

				isoMsgProcess = mappingDataResponse(output, isoMsgProcess);

				log.debug("[SOCKET CLIENT] [Send]" + receiveAddress() + " HEX STRING ["
						+ isoMessageAsHexString(isoMsgProcess) + "]");
				log.info("[SOCKET CLIENT] [Send]" + receiveAddress() + " [" + isoMessageAsString(isoMsgProcess) + "]");
			} else if ("0800".equals(this.isoMsg.getMTI())) {
				isoMsgProcess = mappingDefaultDataResponse(isoMsgProcess);
				log.debug("[SOCKET CLIENT] [Send]" + receiveAddress() + " HEX STRING ["
						+ isoMessageAsHexString(isoMsgProcess) + "]");
				log.info("[SOCKET CLIENT] [Send]" + receiveAddress() + " [" + isoMessageAsString(isoMsgProcess) + "]");
			} else {
				log.info("[SOCKET CLIENT] [Send]" + receiveAddress() + " MESSAGE IGNORE ");
			}
		} catch (Exception e) {
			log.error("[SOCKET CLIENT] " + receiveAddress() + "processsingRequest " + e.getMessage());
		}
		return isoMsgProcess;
	}

	protected void connectionConfig(IDataCursor pipelineCursor) {
		IData log4jConfiguration = IDataUtil.getIData(pipelineCursor, "log4jConfiguration");
		IDataCursor log4jConfigurationCursor = log4jConfiguration.getCursor();
		String log4jConfigFile = IDataUtil.getString(log4jConfigurationCursor, "log4jConfigFile");
		log4jConfigurationCursor.destroy();
		pipelineCursor.destroy();
		Log.init(log4jConfigFile);

		IData socketOutBoundConfig = IDataUtil.getIData(pipelineCursor, "socketOutBoundConfig");
		if (socketOutBoundConfig != null) {
			IDataCursor socketOutBoundConfigCursor = socketOutBoundConfig.getCursor();
			String Backend = IDataUtil.getString(socketOutBoundConfigCursor, "Backend");
			String DestinationIP = IDataUtil.getString(socketOutBoundConfigCursor, "DestinationIP");
			String DestinationPort = IDataUtil.getString(socketOutBoundConfigCursor, "DestinationPort");
			String TimeOut = IDataUtil.getString(socketOutBoundConfigCursor, "TimeOut");
			String reference1 = IDataUtil.getString(socketOutBoundConfigCursor, "reference1");
			String reference3 = IDataUtil.getString(socketOutBoundConfigCursor, "reference3");

			setBackend(Backend);
			setDestinationIP(DestinationIP);
			setDestinationPort(DestinationPort);
			setTimeOut(TimeOut);
			setReference1(reference1);
			setReference3(reference3);

			socketOutBoundConfigCursor.destroy();
		}
		pipelineCursor.destroy();
	}

	protected ISOMsg constructISORequest(IDataCursor pipelineCursor, ISOMsg isoMsg) throws ISOException {
		IData reqMsg = IDataUtil.getIData(pipelineCursor, "reqMsg");
		if (reqMsg != null) {
			IDataCursor reqMsgCursor = reqMsg.getCursor();

			IData msgHeader = IDataUtil.getIData(reqMsgCursor, "msgHeader");
			if (msgHeader != null) {
				IDataCursor msgHeaderCursor = msgHeader.getCursor();
				String msgHeaderHex = IDataUtil.getString(msgHeaderCursor, "msgHeaderHex");
				if (msgHeaderHex != null) {
					isoMsg.setHeader(msgHeaderHex.getBytes());
				}
				msgHeaderCursor.destroy();
			}
			IData msgDetail = IDataUtil.getIData(reqMsgCursor, "msgDetail");
			if (msgDetail != null) {
				IDataCursor msgDetailCursor = msgDetail.getCursor();
				String messageType = IDataUtil.getString(msgDetailCursor, "messageType");
				String primaryAccountNumber = IDataUtil.getString(msgDetailCursor, "primaryAccountNumber");
				String processingCode = IDataUtil.getString(msgDetailCursor, "processingCode");
				String txnAmount = IDataUtil.getString(msgDetailCursor, "txnAmount");
				String settlementAmount = IDataUtil.getString(msgDetailCursor, "settlementAmount");
				String cardholderBillingAmount = IDataUtil.getString(msgDetailCursor, "cardholderBillingAmount");
				String transmissionDatetime = IDataUtil.getString(msgDetailCursor, "transmissionDatetime");
				String settlementConversionRate = IDataUtil.getString(msgDetailCursor, "settlementConversionRate");
				String field9 = IDataUtil.getString(msgDetailCursor, "field9");
				String cardholderBillingConversionRate = IDataUtil.getString(msgDetailCursor,
						"cardholderBillingConversionRate");
				String systemTraceAuditNumber = IDataUtil.getString(msgDetailCursor, "systemTraceAuditNumber");
				String localTxnTime = IDataUtil.getString(msgDetailCursor, "localTxnTime");
				String localTxnDate = IDataUtil.getString(msgDetailCursor, "localTxnDate");
				String expirationDate = IDataUtil.getString(msgDetailCursor, "expirationDate");
				String settlementDate = IDataUtil.getString(msgDetailCursor, "settlementDate");
				String conversionDate = IDataUtil.getString(msgDetailCursor, "conversionDate");
				String field17 = IDataUtil.getString(msgDetailCursor, "field17");
				String merchantType = IDataUtil.getString(msgDetailCursor, "merchantType");
				String acquiringInstitutionCountryCode = IDataUtil.getString(msgDetailCursor,
						"acquiringInstitutionCountryCode");
				String field20 = IDataUtil.getString(msgDetailCursor, "field20");
				String field21 = IDataUtil.getString(msgDetailCursor, "field21");
				String posEntryModeCode = IDataUtil.getString(msgDetailCursor, "posEntryModeCode");
				String cardSequenceNumber = IDataUtil.getString(msgDetailCursor, "cardSequenceNumber");
				String internationalNetworkID = IDataUtil.getString(msgDetailCursor, "internationalNetworkID");
				String posConditionCode = IDataUtil.getString(msgDetailCursor, "posConditionCode");
				String posPinCaptureCode = IDataUtil.getString(msgDetailCursor, "posPinCaptureCode");
				String field27 = IDataUtil.getString(msgDetailCursor, "field27");
				String txnFeeAmount = IDataUtil.getString(msgDetailCursor, "txnFeeAmount");
				String field29 = IDataUtil.getString(msgDetailCursor, "field29");
				String field30 = IDataUtil.getString(msgDetailCursor, "field30");
				String field31 = IDataUtil.getString(msgDetailCursor, "field31");
				String acquiringInstitutionIDCode = IDataUtil.getString(msgDetailCursor, "acquiringInstitutionIDCode");
				String forwardingInstitutionIDCode = IDataUtil.getString(msgDetailCursor,
						"forwardingInstitutionIDCode");
				String field34 = IDataUtil.getString(msgDetailCursor, "field34");
				String track2Data = IDataUtil.getString(msgDetailCursor, "track2Data");
				String track3Data = IDataUtil.getString(msgDetailCursor, "track3Data");
				String retrievalReferenceNumber = IDataUtil.getString(msgDetailCursor, "retrievalReferenceNumber");
				String authorizationIDResponse = IDataUtil.getString(msgDetailCursor, "authorizationIDResponse");
				String responseCode = IDataUtil.getString(msgDetailCursor, "responseCode");
				String field40 = IDataUtil.getString(msgDetailCursor, "field40");
				String cardAcceptorTerminalID = IDataUtil.getString(msgDetailCursor, "cardAcceptorTerminalID");
				String cardAcceptorIDCode = IDataUtil.getString(msgDetailCursor, "cardAcceptorIDCode");
				String cardAcceptorNameLocation = IDataUtil.getString(msgDetailCursor, "cardAcceptorNameLocation");
				String additionalResponseData = IDataUtil.getString(msgDetailCursor, "additionalResponseData");
				String track1Data = IDataUtil.getString(msgDetailCursor, "track1Data");
				String field46 = IDataUtil.getString(msgDetailCursor, "field46");
				String field47 = IDataUtil.getString(msgDetailCursor, "field47");
				String additionalDataPrivate48 = IDataUtil.getString(msgDetailCursor, "additionalDataPrivate48");
				String txnCurrencyCode = IDataUtil.getString(msgDetailCursor, "txnCurrencyCode");
				String settlementCurrencyCode = IDataUtil.getString(msgDetailCursor, "settlementCurrencyCode");
				String cardholderBillingCurrencyCode = IDataUtil.getString(msgDetailCursor,
						"cardholderBillingCurrencyCode");
				Object pin = IDataUtil.get(msgDetailCursor, "pin");
				String securityRelatedControlInfo = IDataUtil.getString(msgDetailCursor, "securityRelatedControlInfo");
				String additionalAmounts = IDataUtil.getString(msgDetailCursor, "additionalAmounts");
				String field55 = IDataUtil.getString(msgDetailCursor, "field55");
				String field56 = IDataUtil.getString(msgDetailCursor, "field56");
				String additionalDataPrivate57 = IDataUtil.getString(msgDetailCursor, "additionalDataPrivate57");
				String icPBOCDataReserved = IDataUtil.getString(msgDetailCursor, "icPBOCDataReserved");
				String detailInquiring = IDataUtil.getString(msgDetailCursor, "detailInquiring");
				String reserved60 = IDataUtil.getString(msgDetailCursor, "reserved60");
				String cardholderAuthenticationInfo = IDataUtil.getString(msgDetailCursor,
						"cardholderAuthenticationInfo");
				String switchingData = IDataUtil.getString(msgDetailCursor, "switchingData");
				String financialNetworkData = IDataUtil.getString(msgDetailCursor, "financialNetworkData");
				Object mac64 = IDataUtil.get(msgDetailCursor, "mac64");
				String field65 = IDataUtil.getString(msgDetailCursor, "field65");
				String settlementCode = IDataUtil.getString(msgDetailCursor, "settlementCode");
				String field67 = IDataUtil.getString(msgDetailCursor, "field67");
				String field68 = IDataUtil.getString(msgDetailCursor, "field68");
				String field69 = IDataUtil.getString(msgDetailCursor, "field69");
				String networkMgtInfoCode = IDataUtil.getString(msgDetailCursor, "networkMgtInfoCode");
				String field71 = IDataUtil.getString(msgDetailCursor, "field71");
				String field72 = IDataUtil.getString(msgDetailCursor, "field72");
				String field73 = IDataUtil.getString(msgDetailCursor, "field73");
				String creditsNumber = IDataUtil.getString(msgDetailCursor, "creditsNumber");
				String creditsReversalNumber = IDataUtil.getString(msgDetailCursor, "creditsReversalNumber");
				String debitsNumber = IDataUtil.getString(msgDetailCursor, "debitsNumber");
				String debitsReversalNumber = IDataUtil.getString(msgDetailCursor, "debitsReversalNumber");
				String transferNumber = IDataUtil.getString(msgDetailCursor, "transferNumber");
				String transferReversalNumber = IDataUtil.getString(msgDetailCursor, "transferReversalNumber");
				String inquiriesNumber = IDataUtil.getString(msgDetailCursor, "inquiriesNumber");
				String authorizationNumber = IDataUtil.getString(msgDetailCursor, "authorizationNumber");
				String creditsProcessingFee = IDataUtil.getString(msgDetailCursor, "creditsProcessingFee");
				String field83 = IDataUtil.getString(msgDetailCursor, "field83");
				String debitsProcessingFee = IDataUtil.getString(msgDetailCursor, "debitsProcessingFee");
				String field85 = IDataUtil.getString(msgDetailCursor, "field85");
				String creditsAmount = IDataUtil.getString(msgDetailCursor, "creditsAmount");
				String creditsReversalAmount = IDataUtil.getString(msgDetailCursor, "creditsReversalAmount");
				String debitsAmount = IDataUtil.getString(msgDetailCursor, "debitsAmount");
				String debutsReversalAmount = IDataUtil.getString(msgDetailCursor, "debutsReversalAmount");
				String origDataElements = IDataUtil.getString(msgDetailCursor, "origDataElements");
				String field91 = IDataUtil.getString(msgDetailCursor, "field91");
				String field92 = IDataUtil.getString(msgDetailCursor, "field92");
				String field93 = IDataUtil.getString(msgDetailCursor, "field93");
				String field94 = IDataUtil.getString(msgDetailCursor, "field94");
				String replacementAmounts = IDataUtil.getString(msgDetailCursor, "replacementAmounts");
				Object messageSecurityCode = IDataUtil.get(msgDetailCursor, "messageSecurityCode");
				String netSettlementAmount = IDataUtil.getString(msgDetailCursor, "netSettlementAmount");
				String field98 = IDataUtil.getString(msgDetailCursor, "field98");
				String settlementInstitutionIDCode = IDataUtil.getString(msgDetailCursor,
						"settlementInstitutionIDCode");
				String receivingInstitutionIDCode = IDataUtil.getString(msgDetailCursor, "receivingInstitutionIDCode");
				String field101 = IDataUtil.getString(msgDetailCursor, "field101");
				String accountID1 = IDataUtil.getString(msgDetailCursor, "accountID1");
				String accountID2 = IDataUtil.getString(msgDetailCursor, "accountID2");
				String txnDescription = IDataUtil.getString(msgDetailCursor, "txnDescription");
				String field105 = IDataUtil.getString(msgDetailCursor, "field105");
				String field106 = IDataUtil.getString(msgDetailCursor, "field106");
				String field107 = IDataUtil.getString(msgDetailCursor, "field107");
				String field108 = IDataUtil.getString(msgDetailCursor, "field108");
				String field109 = IDataUtil.getString(msgDetailCursor, "field109");
				String field110 = IDataUtil.getString(msgDetailCursor, "field110");
				String field111 = IDataUtil.getString(msgDetailCursor, "field111");
				String field112 = IDataUtil.getString(msgDetailCursor, "field112");
				String field113 = IDataUtil.getString(msgDetailCursor, "field113");
				String field114 = IDataUtil.getString(msgDetailCursor, "field114");
				String field115 = IDataUtil.getString(msgDetailCursor, "field115");
				String field116 = IDataUtil.getString(msgDetailCursor, "field116");
				String field117 = IDataUtil.getString(msgDetailCursor, "field117");
				String field118 = IDataUtil.getString(msgDetailCursor, "field118");
				String field119 = IDataUtil.getString(msgDetailCursor, "field119");
				String field120 = IDataUtil.getString(msgDetailCursor, "field120");
				String reserved121 = IDataUtil.getString(msgDetailCursor, "reserved121");
				String acquiringInstitutionReserved = IDataUtil.getString(msgDetailCursor,
						"acquiringInstitutionReserved");
				String issuerInstitutionReserved = IDataUtil.getString(msgDetailCursor, "issuerInstitutionReserved");
				String field124 = IDataUtil.getString(msgDetailCursor, "field124");
				String field125 = IDataUtil.getString(msgDetailCursor, "field125");
				String field126 = IDataUtil.getString(msgDetailCursor, "field126");
				String field127 = IDataUtil.getString(msgDetailCursor, "field127");
				Object mac128 = IDataUtil.get(msgDetailCursor, "mac128");
				if (messageType != null) {
					isoMsg.setMTI(messageType);
				}
				if (primaryAccountNumber != null) {
					isoMsg.set(2, primaryAccountNumber);
				}
				if (processingCode != null) {
					isoMsg.set(3, processingCode);
				}
				if (txnAmount != null) {
					isoMsg.set(4, txnAmount);
				}
				if (settlementAmount != null) {
					isoMsg.set(5, settlementAmount);
				}
				if (cardholderBillingAmount != null) {
					isoMsg.set(6, cardholderBillingAmount);
				}
				if (transmissionDatetime != null) {
					isoMsg.set(7, transmissionDatetime);
				}
				if (settlementConversionRate != null) {
					isoMsg.set(8, settlementConversionRate);
				}
				if (field9 != null) {
					isoMsg.set(9, field9);
				}
				if (cardholderBillingConversionRate != null) {
					isoMsg.set(10, cardholderBillingConversionRate);
				}
				if (systemTraceAuditNumber != null) {
					isoMsg.set(11, systemTraceAuditNumber);
				}
				if (localTxnTime != null) {
					isoMsg.set(12, localTxnTime);
				}
				if (localTxnDate != null) {
					isoMsg.set(13, localTxnDate);
				}
				if (expirationDate != null) {
					isoMsg.set(14, expirationDate);
				}
				if (settlementDate != null) {
					isoMsg.set(15, settlementDate);
				}
				if (conversionDate != null) {
					isoMsg.set(16, conversionDate);
				}
				if (field17 != null) {
					isoMsg.set(17, field17);
				}
				if (merchantType != null) {
					isoMsg.set(18, merchantType);
				}
				if (acquiringInstitutionCountryCode != null) {
					isoMsg.set(19, acquiringInstitutionCountryCode);
				}
				if (field20 != null) {
					isoMsg.set(20, field20);
				}
				if (field21 != null) {
					isoMsg.set(21, field21);
				}
				if (posEntryModeCode != null) {
					isoMsg.set(22, posEntryModeCode);
				}
				if (cardSequenceNumber != null) {
					isoMsg.set(23, cardSequenceNumber);
				}
				if (internationalNetworkID != null) {
					isoMsg.set(24, internationalNetworkID);
				}
				if (posConditionCode != null) {
					isoMsg.set(25, posConditionCode);
				}
				if (posPinCaptureCode != null) {
					isoMsg.set(26, posPinCaptureCode);
				}
				if (field27 != null) {
					isoMsg.set(27, field27);
				}
				if (txnFeeAmount != null) {
					isoMsg.set(28, txnFeeAmount);
				}
				if (field29 != null) {
					isoMsg.set(29, field29);
				}
				if (field30 != null) {
					isoMsg.set(30, field30);
				}
				if (field31 != null) {
					isoMsg.set(31, field31);
				}
				if (acquiringInstitutionIDCode != null) {
					isoMsg.set(32, acquiringInstitutionIDCode);
				}
				if (forwardingInstitutionIDCode != null) {
					isoMsg.set(33, forwardingInstitutionIDCode);
				}
				if (field34 != null) {
					isoMsg.set(34, field34);
				}
				if (track2Data != null) {
					isoMsg.set(35, track2Data);
				}
				if (track3Data != null) {
					isoMsg.set(36, track3Data);
				}
				if (retrievalReferenceNumber != null) {
					isoMsg.set(37, retrievalReferenceNumber);
				}
				if (authorizationIDResponse != null) {
					isoMsg.set(38, authorizationIDResponse);
				}
				if (responseCode != null) {
					isoMsg.set(39, responseCode);
				}
				if (field40 != null) {
					isoMsg.set(40, field40);
				}
				if (cardAcceptorTerminalID != null) {
					isoMsg.set(41, cardAcceptorTerminalID);
				}
				if (cardAcceptorIDCode != null) {
					isoMsg.set(42, cardAcceptorIDCode);
				}
				if (cardAcceptorNameLocation != null) {
					isoMsg.set(43, cardAcceptorNameLocation);
				}
				if (additionalResponseData != null) {
					isoMsg.set(44, additionalResponseData);
				}
				if (track1Data != null) {
					isoMsg.set(45, track1Data);
				}
				if (field46 != null) {
					isoMsg.set(46, field46);
				}
				if (field47 != null) {
					isoMsg.set(47, field47);
				}
				if (additionalDataPrivate48 != null) {
					isoMsg.set(48, additionalDataPrivate48);
				}
				if (txnCurrencyCode != null) {
					isoMsg.set(49, txnCurrencyCode);
				}
				if (settlementCurrencyCode != null) {
					isoMsg.set(50, settlementCurrencyCode);
				}
				if (cardholderBillingCurrencyCode != null) {
					isoMsg.set(51, cardholderBillingCurrencyCode);
				}
				if (pin != null) {
					isoMsg.set(52, (String) pin);
				}
				if (securityRelatedControlInfo != null) {
					isoMsg.set(53, securityRelatedControlInfo);
				}
				if (additionalAmounts != null) {
					isoMsg.set(54, additionalAmounts);
				}
				if (field55 != null) {
					isoMsg.set(55, field55);
				}
				if (field56 != null) {
					isoMsg.set(56, field56);
				}
				if (additionalDataPrivate57 != null) {
					isoMsg.set(57, additionalDataPrivate57);
				}
				if (icPBOCDataReserved != null) {
					isoMsg.set(58, icPBOCDataReserved);
				}
				if (detailInquiring != null) {
					isoMsg.set(59, detailInquiring);
				}
				if (reserved60 != null) {
					isoMsg.set(60, reserved60);
				}
				if (cardholderAuthenticationInfo != null) {
					isoMsg.set(61, cardholderAuthenticationInfo);
				}
				if (switchingData != null) {
					isoMsg.set(62, switchingData);
				}
				if (financialNetworkData != null) {
					isoMsg.set(63, financialNetworkData);
				}
				if (mac64 != null) {
					isoMsg.set(64, (String) mac64);
				}
				if (field65 != null) {
					isoMsg.set(65, field65);
				}
				if (settlementCode != null) {
					isoMsg.set(66, settlementCode);
				}
				if (field67 != null) {
					isoMsg.set(67, field67);
				}
				if (field68 != null) {
					isoMsg.set(68, field68);
				}
				if (field69 != null) {
					isoMsg.set(69, field69);
				}
				if (networkMgtInfoCode != null) {
					isoMsg.set(70, networkMgtInfoCode);
				}
				if (field71 != null) {
					isoMsg.set(71, field71);
				}
				if (field72 != null) {
					isoMsg.set(72, field72);
				}
				if (field73 != null) {
					isoMsg.set(73, field73);
				}
				if (creditsNumber != null) {
					isoMsg.set(74, creditsNumber);
				}
				if (creditsReversalNumber != null) {
					isoMsg.set(75, creditsReversalNumber);
				}
				if (debitsNumber != null) {
					isoMsg.set(76, debitsNumber);
				}
				if (debitsReversalNumber != null) {
					isoMsg.set(77, debitsReversalNumber);
				}
				if (transferNumber != null) {
					isoMsg.set(78, transferNumber);
				}
				if (transferReversalNumber != null) {
					isoMsg.set(79, transferReversalNumber);
				}
				if (inquiriesNumber != null) {
					isoMsg.set(80, inquiriesNumber);
				}
				if (authorizationNumber != null) {
					isoMsg.set(81, authorizationNumber);
				}
				if (creditsProcessingFee != null) {
					isoMsg.set(82, creditsProcessingFee);
				}
				if (field83 != null) {
					isoMsg.set(83, field83);
				}
				if (debitsProcessingFee != null) {
					isoMsg.set(84, debitsProcessingFee);
				}
				if (field85 != null) {
					isoMsg.set(85, field85);
				}
				if (creditsAmount != null) {
					isoMsg.set(86, creditsAmount);
				}
				if (creditsReversalAmount != null) {
					isoMsg.set(87, creditsReversalAmount);
				}
				if (debitsAmount != null) {
					isoMsg.set(88, debitsAmount);
				}
				if (debutsReversalAmount != null) {
					isoMsg.set(89, debutsReversalAmount);
				}
				if (origDataElements != null) {
					isoMsg.set(90, origDataElements);
				}
				if (field91 != null) {
					isoMsg.set(91, field91);
				}
				if (field92 != null) {
					isoMsg.set(92, field92);
				}
				if (field93 != null) {
					isoMsg.set(93, field93);
				}
				if (field94 != null) {
					isoMsg.set(94, field94);
				}
				if (replacementAmounts != null) {
					isoMsg.set(95, replacementAmounts);
				}
				if (messageSecurityCode != null) {
					isoMsg.set(96, (String) messageSecurityCode);
				}
				if (netSettlementAmount != null) {
					isoMsg.set(97, netSettlementAmount);
				}
				if (field98 != null) {
					isoMsg.set(98, field98);
				}
				if (settlementInstitutionIDCode != null) {
					isoMsg.set(99, settlementInstitutionIDCode);
				}
				if (receivingInstitutionIDCode != null) {
					isoMsg.set(100, receivingInstitutionIDCode);
				}
				if (field101 != null) {
					isoMsg.set(101, field101);
				}
				if (accountID1 != null) {
					isoMsg.set(102, accountID1);
				}
				if (accountID2 != null) {
					isoMsg.set(103, accountID2);
				}
				if (txnDescription != null) {
					isoMsg.set(104, txnDescription);
				}
				if (field105 != null) {
					isoMsg.set(105, field105);
				}
				if (field106 != null) {
					isoMsg.set(106, field106);
				}
				if (field107 != null) {
					isoMsg.set(107, field107);
				}
				if (field108 != null) {
					isoMsg.set(108, field108);
				}
				if (field109 != null) {
					isoMsg.set(109, field109);
				}
				if (field110 != null) {
					isoMsg.set(110, field110);
				}
				if (field111 != null) {
					isoMsg.set(111, field111);
				}
				if (field112 != null) {
					isoMsg.set(112, field112);
				}
				if (field113 != null) {
					isoMsg.set(113, field113);
				}
				if (field114 != null) {
					isoMsg.set(114, field114);
				}
				if (field115 != null) {
					isoMsg.set(115, field115);
				}
				if (field116 != null) {
					isoMsg.set(116, field116);
				}
				if (field117 != null) {
					isoMsg.set(117, field117);
				}
				if (field118 != null) {
					isoMsg.set(118, field118);
				}
				if (field119 != null) {
					isoMsg.set(119, field119);
				}
				if (field120 != null) {
					isoMsg.set(120, field120);
				}
				if (reserved121 != null) {
					isoMsg.set(121, reserved121);
				}
				if (acquiringInstitutionReserved != null) {
					isoMsg.set(122, acquiringInstitutionReserved);
				}
				if (issuerInstitutionReserved != null) {
					isoMsg.set(123, issuerInstitutionReserved);
				}
				if (field124 != null) {
					isoMsg.set(124, field124);
				}
				if (field125 != null) {
					isoMsg.set(125, field125);
				}
				if (field126 != null) {
					isoMsg.set(126, field126);
				}
				if (field127 != null) {
					isoMsg.set(127, field127);
				}
				if (mac128 != null) {
					isoMsg.set(128, (String) mac128);
				}
				msgDetailCursor.destroy();
			}
			reqMsgCursor.destroy();
		}
		return isoMsg;
	}

	protected void constructISOResponse(IDataCursor pipelineCursor_1, ISOMsg isoMsg) throws ISOException {
		IData respMsg = IDataFactory.create();
		IDataCursor respMsgCursor = respMsg.getCursor();
		IDataUtil.put(respMsgCursor, "raw", isoMessageAsString(isoMsg));

		IData msgHeader_1 = IDataFactory.create();
		IDataCursor msgHeader_1Cursor = msgHeader_1.getCursor();
		IDataUtil.put(msgHeader_1Cursor, "msgHeaderHex", setISOHeader(isoMsg));
		msgHeader_1Cursor.destroy();
		IDataUtil.put(respMsgCursor, "msgHeader", msgHeader_1);

		IData msgDetail_1 = IDataFactory.create();
		IDataCursor msgDetail_1Cursor = msgDetail_1.getCursor();
		IDataUtil.put(msgDetail_1Cursor, "messageType", isoMsg.getMTI());
		IDataUtil.put(msgDetail_1Cursor, "primaryAccountNumber", isoMsg.getString(2));
		IDataUtil.put(msgDetail_1Cursor, "processingCode", isoMsg.getString(3));
		IDataUtil.put(msgDetail_1Cursor, "txnAmount", isoMsg.getString(4));
		IDataUtil.put(msgDetail_1Cursor, "settlementAmount", isoMsg.getString(5));
		IDataUtil.put(msgDetail_1Cursor, "cardholderBillingAmount", isoMsg.getString(6));
		IDataUtil.put(msgDetail_1Cursor, "transmissionDatetime", isoMsg.getString(7));
		IDataUtil.put(msgDetail_1Cursor, "settlementConversionRate", isoMsg.getString(8));
		IDataUtil.put(msgDetail_1Cursor, "field9", isoMsg.getString(9));
		IDataUtil.put(msgDetail_1Cursor, "cardholderBillingConversionRate", isoMsg.getString(10));
		IDataUtil.put(msgDetail_1Cursor, "systemTraceAuditNumber", isoMsg.getString(11));
		IDataUtil.put(msgDetail_1Cursor, "localTxnTime", isoMsg.getString(12));
		IDataUtil.put(msgDetail_1Cursor, "localTxnDate", isoMsg.getString(13));
		IDataUtil.put(msgDetail_1Cursor, "expirationDate", isoMsg.getString(14));
		IDataUtil.put(msgDetail_1Cursor, "settlementDate", isoMsg.getString(15));
		IDataUtil.put(msgDetail_1Cursor, "conversionDate", isoMsg.getString(16));
		IDataUtil.put(msgDetail_1Cursor, "field17", isoMsg.getString(17));
		IDataUtil.put(msgDetail_1Cursor, "merchantType", isoMsg.getString(18));
		IDataUtil.put(msgDetail_1Cursor, "acquiringInstitutionCountryCode", isoMsg.getString(19));
		IDataUtil.put(msgDetail_1Cursor, "field20", isoMsg.getString(20));
		IDataUtil.put(msgDetail_1Cursor, "field21", isoMsg.getString(21));
		IDataUtil.put(msgDetail_1Cursor, "posEntryModeCode", isoMsg.getString(22));
		IDataUtil.put(msgDetail_1Cursor, "cardSequenceNumber", isoMsg.getString(23));
		IDataUtil.put(msgDetail_1Cursor, "internationalNetworkID", isoMsg.getString(24));
		IDataUtil.put(msgDetail_1Cursor, "posConditionCode", isoMsg.getString(25));
		IDataUtil.put(msgDetail_1Cursor, "posPinCaptureCode", isoMsg.getString(26));
		IDataUtil.put(msgDetail_1Cursor, "field27", isoMsg.getString(27));
		IDataUtil.put(msgDetail_1Cursor, "txnFeeAmount", isoMsg.getString(28));
		IDataUtil.put(msgDetail_1Cursor, "field29", isoMsg.getString(29));
		IDataUtil.put(msgDetail_1Cursor, "field30", isoMsg.getString(30));
		IDataUtil.put(msgDetail_1Cursor, "field31", isoMsg.getString(31));
		IDataUtil.put(msgDetail_1Cursor, "acquiringInstitutionIDCode", isoMsg.getString(32));
		IDataUtil.put(msgDetail_1Cursor, "forwardingInstitutionIDCode", isoMsg.getString(33));
		IDataUtil.put(msgDetail_1Cursor, "field34", isoMsg.getString(34));
		IDataUtil.put(msgDetail_1Cursor, "track2Data", isoMsg.getString(35));
		IDataUtil.put(msgDetail_1Cursor, "track3Data", isoMsg.getString(36));
		IDataUtil.put(msgDetail_1Cursor, "retrievalReferenceNumber", isoMsg.getString(37));
		IDataUtil.put(msgDetail_1Cursor, "authorizationIDResponse", isoMsg.getString(38));
		IDataUtil.put(msgDetail_1Cursor, "responseCode", isoMsg.getString(39));
		IDataUtil.put(msgDetail_1Cursor, "field40", isoMsg.getString(40));
		IDataUtil.put(msgDetail_1Cursor, "cardAcceptorTerminalID", isoMsg.getString(41));
		IDataUtil.put(msgDetail_1Cursor, "cardAcceptorIDCode", isoMsg.getString(42));
		IDataUtil.put(msgDetail_1Cursor, "cardAcceptorNameLocation", isoMsg.getString(43));
		IDataUtil.put(msgDetail_1Cursor, "additionalResponseData", isoMsg.getString(44));
		IDataUtil.put(msgDetail_1Cursor, "track1Data", isoMsg.getString(45));
		IDataUtil.put(msgDetail_1Cursor, "field46", isoMsg.getString(46));
		IDataUtil.put(msgDetail_1Cursor, "field47", isoMsg.getString(47));
		IDataUtil.put(msgDetail_1Cursor, "additionalDataPrivate48", isoMsg.getString(48));
		IDataUtil.put(msgDetail_1Cursor, "txnCurrencyCode", isoMsg.getString(49));
		IDataUtil.put(msgDetail_1Cursor, "settlementCurrencyCode", isoMsg.getString(50));
		IDataUtil.put(msgDetail_1Cursor, "cardholderBillingCurrencyCode", isoMsg.getString(51));
		IDataUtil.put(msgDetail_1Cursor, "pin", isoMsg.getString(52));
		IDataUtil.put(msgDetail_1Cursor, "securityRelatedControlInfo", isoMsg.getString(53));
		IDataUtil.put(msgDetail_1Cursor, "additionalAmounts", isoMsg.getString(54));
		IDataUtil.put(msgDetail_1Cursor, "field55", isoMsg.getString(55));
		IDataUtil.put(msgDetail_1Cursor, "field56", isoMsg.getString(56));
		IDataUtil.put(msgDetail_1Cursor, "additionalDataPrivate57", isoMsg.getString(57));
		IDataUtil.put(msgDetail_1Cursor, "icPBOCDataReserved", isoMsg.getString(58));
		IDataUtil.put(msgDetail_1Cursor, "detailInquiring", isoMsg.getString(59));
		IDataUtil.put(msgDetail_1Cursor, "reserved60", isoMsg.getString(60));
		IDataUtil.put(msgDetail_1Cursor, "cardholderAuthenticationInfo", isoMsg.getString(61));
		IDataUtil.put(msgDetail_1Cursor, "switchingData", isoMsg.getString(62));
		IDataUtil.put(msgDetail_1Cursor, "financialNetworkData", isoMsg.getString(63));
		IDataUtil.put(msgDetail_1Cursor, "mac64", isoMsg.getString(64));
		IDataUtil.put(msgDetail_1Cursor, "field65", isoMsg.getString(65));
		IDataUtil.put(msgDetail_1Cursor, "settlementCode", isoMsg.getString(66));
		IDataUtil.put(msgDetail_1Cursor, "field67", isoMsg.getString(67));
		IDataUtil.put(msgDetail_1Cursor, "field68", isoMsg.getString(68));
		IDataUtil.put(msgDetail_1Cursor, "field69", isoMsg.getString(69));
		IDataUtil.put(msgDetail_1Cursor, "networkMgtInfoCode", isoMsg.getString(70));
		IDataUtil.put(msgDetail_1Cursor, "field71", isoMsg.getString(71));
		IDataUtil.put(msgDetail_1Cursor, "field72", isoMsg.getString(72));
		IDataUtil.put(msgDetail_1Cursor, "field73", isoMsg.getString(73));
		IDataUtil.put(msgDetail_1Cursor, "creditsNumber", isoMsg.getString(74));
		IDataUtil.put(msgDetail_1Cursor, "creditsReversalNumber", isoMsg.getString(75));
		IDataUtil.put(msgDetail_1Cursor, "debitsNumber", isoMsg.getString(76));
		IDataUtil.put(msgDetail_1Cursor, "debitsReversalNumber", isoMsg.getString(77));
		IDataUtil.put(msgDetail_1Cursor, "transferNumber", isoMsg.getString(78));
		IDataUtil.put(msgDetail_1Cursor, "transferReversalNumber", isoMsg.getString(79));
		IDataUtil.put(msgDetail_1Cursor, "inquiriesNumber", isoMsg.getString(80));
		IDataUtil.put(msgDetail_1Cursor, "authorizationNumber", isoMsg.getString(81));
		IDataUtil.put(msgDetail_1Cursor, "creditsProcessingFee", isoMsg.getString(82));
		IDataUtil.put(msgDetail_1Cursor, "field83", isoMsg.getString(83));
		IDataUtil.put(msgDetail_1Cursor, "debitsProcessingFee", isoMsg.getString(84));
		IDataUtil.put(msgDetail_1Cursor, "field85", isoMsg.getString(85));
		IDataUtil.put(msgDetail_1Cursor, "creditsAmount", isoMsg.getString(86));
		IDataUtil.put(msgDetail_1Cursor, "creditsReversalAmount", isoMsg.getString(87));
		IDataUtil.put(msgDetail_1Cursor, "debitsAmount", isoMsg.getString(88));
		IDataUtil.put(msgDetail_1Cursor, "debutsReversalAmount", isoMsg.getString(89));
		IDataUtil.put(msgDetail_1Cursor, "origDataElements", isoMsg.getString(90));
		IDataUtil.put(msgDetail_1Cursor, "field91", isoMsg.getString(91));
		IDataUtil.put(msgDetail_1Cursor, "field92", isoMsg.getString(92));
		IDataUtil.put(msgDetail_1Cursor, "field93", isoMsg.getString(93));
		IDataUtil.put(msgDetail_1Cursor, "field94", isoMsg.getString(94));
		IDataUtil.put(msgDetail_1Cursor, "replacementAmounts", isoMsg.getString(95));
		IDataUtil.put(msgDetail_1Cursor, "messageSecurityCode", isoMsg.getString(96));
		IDataUtil.put(msgDetail_1Cursor, "netSettlementAmount", isoMsg.getString(97));
		IDataUtil.put(msgDetail_1Cursor, "field98", isoMsg.getString(98));
		IDataUtil.put(msgDetail_1Cursor, "settlementInstitutionIDCode", isoMsg.getString(99));
		IDataUtil.put(msgDetail_1Cursor, "receivingInstitutionIDCode", isoMsg.getString(100));
		IDataUtil.put(msgDetail_1Cursor, "field101", isoMsg.getString(101));
		IDataUtil.put(msgDetail_1Cursor, "accountID1", isoMsg.getString(102));
		IDataUtil.put(msgDetail_1Cursor, "accountID2", isoMsg.getString(103));
		IDataUtil.put(msgDetail_1Cursor, "txnDescription", isoMsg.getString(104));
		IDataUtil.put(msgDetail_1Cursor, "field105", isoMsg.getString(105));
		IDataUtil.put(msgDetail_1Cursor, "field106", isoMsg.getString(106));
		IDataUtil.put(msgDetail_1Cursor, "field107", isoMsg.getString(107));
		IDataUtil.put(msgDetail_1Cursor, "field108", isoMsg.getString(108));
		IDataUtil.put(msgDetail_1Cursor, "field109", isoMsg.getString(109));
		IDataUtil.put(msgDetail_1Cursor, "field110", isoMsg.getString(110));
		IDataUtil.put(msgDetail_1Cursor, "field111", isoMsg.getString(111));
		IDataUtil.put(msgDetail_1Cursor, "field112", isoMsg.getString(112));
		IDataUtil.put(msgDetail_1Cursor, "field113", isoMsg.getString(113));
		IDataUtil.put(msgDetail_1Cursor, "field114", isoMsg.getString(114));
		IDataUtil.put(msgDetail_1Cursor, "field115", isoMsg.getString(115));
		IDataUtil.put(msgDetail_1Cursor, "field116", isoMsg.getString(116));
		IDataUtil.put(msgDetail_1Cursor, "field117", isoMsg.getString(117));
		IDataUtil.put(msgDetail_1Cursor, "field118", isoMsg.getString(118));
		IDataUtil.put(msgDetail_1Cursor, "field119", isoMsg.getString(119));
		IDataUtil.put(msgDetail_1Cursor, "field120", isoMsg.getString(120));
		IDataUtil.put(msgDetail_1Cursor, "reserved121", isoMsg.getString(121));
		IDataUtil.put(msgDetail_1Cursor, "acquiringInstitutionReserved", isoMsg.getString(122));
		IDataUtil.put(msgDetail_1Cursor, "issuerInstitutionReserved", isoMsg.getString(123));
		IDataUtil.put(msgDetail_1Cursor, "field124", isoMsg.getString(124));
		IDataUtil.put(msgDetail_1Cursor, "field125", isoMsg.getString(125));
		IDataUtil.put(msgDetail_1Cursor, "field126", isoMsg.getString(126));
		IDataUtil.put(msgDetail_1Cursor, "field127", isoMsg.getString(127));
		IDataUtil.put(msgDetail_1Cursor, "mac128", isoMsg.getString(128));

		msgDetail_1Cursor.destroy();
		IDataUtil.put(respMsgCursor, "msgDetail", msgDetail_1);
		respMsgCursor.destroy();
		IDataUtil.put(pipelineCursor_1, "respMsg", respMsg);
	}

	protected IData mappingDataRequest(ISOSource isoSource, ISOMsg isoMessage) throws ISOException {
		IData input = IDataFactory.create();
		IDataCursor inputCursor = input.getCursor();

		IData reqMsg = IDataFactory.create();
		IDataCursor reqMsgCursor = reqMsg.getCursor();
		IDataUtil.put(reqMsgCursor, "raw", isoMessageAsString(isoMessage));

		IData msgHeader = IDataFactory.create();
		IDataCursor msgHeaderCursor = msgHeader.getCursor();
		IDataUtil.put(msgHeaderCursor, "msgHeaderHex", setISOHeader(isoMessage));
		msgHeaderCursor.destroy();
		IDataUtil.put(reqMsgCursor, "msgHeader", msgHeader);

		IData msgDetail = IDataFactory.create();
		IDataCursor msgDetailCursor = msgDetail.getCursor();
		IDataUtil.put(msgDetailCursor, "messageType", isoMessage.getMTI());
		IDataUtil.put(msgDetailCursor, "primaryAccountNumber", isoMessage.getString(2));
		IDataUtil.put(msgDetailCursor, "processingCode", isoMessage.getString(3));
		IDataUtil.put(msgDetailCursor, "txnAmount", isoMessage.getString(4));
		IDataUtil.put(msgDetailCursor, "settlementAmount", isoMessage.getString(5));
		IDataUtil.put(msgDetailCursor, "cardholderBillingAmount", isoMessage.getString(6));
		IDataUtil.put(msgDetailCursor, "transmissionDatetime", isoMessage.getString(7));
		IDataUtil.put(msgDetailCursor, "settlementConversionRate", isoMessage.getString(8));
		IDataUtil.put(msgDetailCursor, "field9", isoMessage.getString(9));
		IDataUtil.put(msgDetailCursor, "cardholderBillingConversionRate", isoMessage.getString(10));
		IDataUtil.put(msgDetailCursor, "systemTraceAuditNumber", isoMessage.getString(11));
		IDataUtil.put(msgDetailCursor, "localTxnTime", isoMessage.getString(12));
		IDataUtil.put(msgDetailCursor, "localTxnDate", isoMessage.getString(13));
		IDataUtil.put(msgDetailCursor, "expirationDate", isoMessage.getString(14));
		IDataUtil.put(msgDetailCursor, "settlementDate", isoMessage.getString(15));
		IDataUtil.put(msgDetailCursor, "conversionDate", isoMessage.getString(16));
		IDataUtil.put(msgDetailCursor, "field17", isoMessage.getString(17));
		IDataUtil.put(msgDetailCursor, "merchantType", isoMessage.getString(18));
		IDataUtil.put(msgDetailCursor, "acquiringInstitutionCountryCode", isoMessage.getString(19));
		IDataUtil.put(msgDetailCursor, "field20", isoMessage.getString(20));
		IDataUtil.put(msgDetailCursor, "field21", isoMessage.getString(21));
		IDataUtil.put(msgDetailCursor, "posEntryModeCode", isoMessage.getString(22));
		IDataUtil.put(msgDetailCursor, "cardSequenceNumber", isoMessage.getString(23));
		IDataUtil.put(msgDetailCursor, "internationalNetworkID", isoMessage.getString(24));
		IDataUtil.put(msgDetailCursor, "posConditionCode", isoMessage.getString(25));
		IDataUtil.put(msgDetailCursor, "posPinCaptureCode", isoMessage.getString(26));
		IDataUtil.put(msgDetailCursor, "field27", isoMessage.getString(27));
		IDataUtil.put(msgDetailCursor, "txnFeeAmount", isoMessage.getString(28));
		IDataUtil.put(msgDetailCursor, "field29", isoMessage.getString(29));
		IDataUtil.put(msgDetailCursor, "field30", isoMessage.getString(30));
		IDataUtil.put(msgDetailCursor, "field31", isoMessage.getString(31));
		IDataUtil.put(msgDetailCursor, "acquiringInstitutionIDCode", isoMessage.getString(32));
		IDataUtil.put(msgDetailCursor, "forwardingInstitutionIDCode", isoMessage.getString(33));
		IDataUtil.put(msgDetailCursor, "field34", isoMessage.getString(34));
		IDataUtil.put(msgDetailCursor, "track2Data", isoMessage.getString(35));
		IDataUtil.put(msgDetailCursor, "track3Data", isoMessage.getString(36));
		IDataUtil.put(msgDetailCursor, "retrievalReferenceNumber", isoMessage.getString(37));
		IDataUtil.put(msgDetailCursor, "authorizationIDResponse", isoMessage.getString(38));
		IDataUtil.put(msgDetailCursor, "responseCode", isoMessage.getString(39));
		IDataUtil.put(msgDetailCursor, "field40", isoMessage.getString(40));
		IDataUtil.put(msgDetailCursor, "cardAcceptorTerminalID", isoMessage.getString(41));
		IDataUtil.put(msgDetailCursor, "cardAcceptorIDCode", isoMessage.getString(42));
		IDataUtil.put(msgDetailCursor, "cardAcceptorNameLocation", isoMessage.getString(43));
		IDataUtil.put(msgDetailCursor, "additionalResponseData", isoMessage.getString(44));
		IDataUtil.put(msgDetailCursor, "track1Data", isoMessage.getString(45));
		IDataUtil.put(msgDetailCursor, "field46", isoMessage.getString(46));
		IDataUtil.put(msgDetailCursor, "field47", isoMessage.getString(47));
		IDataUtil.put(msgDetailCursor, "additionalDataPrivate48", isoMessage.getString(48));
		IDataUtil.put(msgDetailCursor, "txnCurrencyCode", isoMessage.getString(49));
		IDataUtil.put(msgDetailCursor, "settlementCurrencyCode", isoMessage.getString(50));
		IDataUtil.put(msgDetailCursor, "cardholderBillingCurrencyCode", isoMessage.getString(51));
		IDataUtil.put(msgDetailCursor, "pin", isoMessage.getString(52));
		IDataUtil.put(msgDetailCursor, "securityRelatedControlInfo", isoMessage.getString(53));
		IDataUtil.put(msgDetailCursor, "additionalAmounts", isoMessage.getString(54));
		IDataUtil.put(msgDetailCursor, "field55", isoMessage.getString(55));
		IDataUtil.put(msgDetailCursor, "field56", isoMessage.getString(56));
		IDataUtil.put(msgDetailCursor, "additionalDataPrivate57", isoMessage.getString(57));
		IDataUtil.put(msgDetailCursor, "icPBOCDataReserved", isoMessage.getString(58));
		IDataUtil.put(msgDetailCursor, "detailInquiring", isoMessage.getString(59));
		IDataUtil.put(msgDetailCursor, "reserved60", isoMessage.getString(60));
		IDataUtil.put(msgDetailCursor, "cardholderAuthenticationInfo", isoMessage.getString(61));
		IDataUtil.put(msgDetailCursor, "switchingData", isoMessage.getString(62));
		IDataUtil.put(msgDetailCursor, "financialNetworkData", isoMessage.getString(63));
		IDataUtil.put(msgDetailCursor, "mac64", isoMessage.getString(64));
		IDataUtil.put(msgDetailCursor, "field65", isoMessage.getString(65));
		IDataUtil.put(msgDetailCursor, "settlementCode", isoMessage.getString(66));
		IDataUtil.put(msgDetailCursor, "field67", isoMessage.getString(67));
		IDataUtil.put(msgDetailCursor, "field68", isoMessage.getString(68));
		IDataUtil.put(msgDetailCursor, "field69", isoMessage.getString(69));
		IDataUtil.put(msgDetailCursor, "networkMgtInfoCode", isoMessage.getString(70));
		IDataUtil.put(msgDetailCursor, "field71", isoMessage.getString(71));
		IDataUtil.put(msgDetailCursor, "field72", isoMessage.getString(72));
		IDataUtil.put(msgDetailCursor, "field73", isoMessage.getString(73));
		IDataUtil.put(msgDetailCursor, "creditsNumber", isoMessage.getString(74));
		IDataUtil.put(msgDetailCursor, "creditsReversalNumber", isoMessage.getString(75));
		IDataUtil.put(msgDetailCursor, "debitsNumber", isoMessage.getString(76));
		IDataUtil.put(msgDetailCursor, "debitsReversalNumber", isoMessage.getString(77));
		IDataUtil.put(msgDetailCursor, "transferNumber", isoMessage.getString(78));
		IDataUtil.put(msgDetailCursor, "transferReversalNumber", isoMessage.getString(79));
		IDataUtil.put(msgDetailCursor, "inquiriesNumber", isoMessage.getString(80));
		IDataUtil.put(msgDetailCursor, "authorizationNumber", isoMessage.getString(81));
		IDataUtil.put(msgDetailCursor, "creditsProcessingFee", isoMessage.getString(82));
		IDataUtil.put(msgDetailCursor, "field83", isoMessage.getString(83));
		IDataUtil.put(msgDetailCursor, "debitsProcessingFee", isoMessage.getString(84));
		IDataUtil.put(msgDetailCursor, "field85", isoMessage.getString(85));
		IDataUtil.put(msgDetailCursor, "creditsAmount", isoMessage.getString(86));
		IDataUtil.put(msgDetailCursor, "creditsReversalAmount", isoMessage.getString(87));
		IDataUtil.put(msgDetailCursor, "debitsAmount", isoMessage.getString(88));
		IDataUtil.put(msgDetailCursor, "debutsReversalAmount", isoMessage.getString(89));
		IDataUtil.put(msgDetailCursor, "origDataElements", isoMessage.getString(90));
		IDataUtil.put(msgDetailCursor, "field91", isoMessage.getString(91));
		IDataUtil.put(msgDetailCursor, "field92", isoMessage.getString(92));
		IDataUtil.put(msgDetailCursor, "field93", isoMessage.getString(93));
		IDataUtil.put(msgDetailCursor, "field94", isoMessage.getString(94));
		IDataUtil.put(msgDetailCursor, "replacementAmounts", isoMessage.getString(95));
		IDataUtil.put(msgDetailCursor, "messageSecurityCode", isoMessage.getString(96));
		IDataUtil.put(msgDetailCursor, "netSettlementAmount", isoMessage.getString(97));
		IDataUtil.put(msgDetailCursor, "field98", isoMessage.getString(98));
		IDataUtil.put(msgDetailCursor, "settlementInstitutionIDCode", isoMessage.getString(99));
		IDataUtil.put(msgDetailCursor, "receivingInstitutionIDCode", isoMessage.getString(100));
		IDataUtil.put(msgDetailCursor, "field101", isoMessage.getString(101));
		IDataUtil.put(msgDetailCursor, "accountID1", isoMessage.getString(102));
		IDataUtil.put(msgDetailCursor, "accountID2", isoMessage.getString(103));
		IDataUtil.put(msgDetailCursor, "txnDescription", isoMessage.getString(104));
		IDataUtil.put(msgDetailCursor, "field105", isoMessage.getString(105));
		IDataUtil.put(msgDetailCursor, "field106", isoMessage.getString(106));
		IDataUtil.put(msgDetailCursor, "field107", isoMessage.getString(107));
		IDataUtil.put(msgDetailCursor, "field108", isoMessage.getString(108));
		IDataUtil.put(msgDetailCursor, "field109", isoMessage.getString(109));
		IDataUtil.put(msgDetailCursor, "field110", isoMessage.getString(110));
		IDataUtil.put(msgDetailCursor, "field111", isoMessage.getString(111));
		IDataUtil.put(msgDetailCursor, "field112", isoMessage.getString(112));
		IDataUtil.put(msgDetailCursor, "field113", isoMessage.getString(113));
		IDataUtil.put(msgDetailCursor, "field114", isoMessage.getString(114));
		IDataUtil.put(msgDetailCursor, "field115", isoMessage.getString(115));
		IDataUtil.put(msgDetailCursor, "field116", isoMessage.getString(116));
		IDataUtil.put(msgDetailCursor, "field117", isoMessage.getString(117));
		IDataUtil.put(msgDetailCursor, "field118", isoMessage.getString(118));
		IDataUtil.put(msgDetailCursor, "field119", isoMessage.getString(119));
		IDataUtil.put(msgDetailCursor, "field120", isoMessage.getString(120));
		IDataUtil.put(msgDetailCursor, "reserved121", isoMessage.getString(121));
		IDataUtil.put(msgDetailCursor, "acquiringInstitutionReserved", isoMessage.getString(122));
		IDataUtil.put(msgDetailCursor, "issuerInstitutionReserved", isoMessage.getString(123));
		IDataUtil.put(msgDetailCursor, "field124", isoMessage.getString(124));
		IDataUtil.put(msgDetailCursor, "field125", isoMessage.getString(125));
		IDataUtil.put(msgDetailCursor, "field126", isoMessage.getString(126));
		IDataUtil.put(msgDetailCursor, "field127", isoMessage.getString(127));
		IDataUtil.put(msgDetailCursor, "mac128", isoMessage.getString(128));

		msgDetailCursor.destroy();
		IDataUtil.put(reqMsgCursor, "msgDetail", msgDetail);
		reqMsgCursor.destroy();
		IDataUtil.put(inputCursor, "reqMsg", reqMsg);

		IData connectionInfo = IDataFactory.create();
		IDataCursor connectionInfoCursor = connectionInfo.getCursor();
		IDataUtil.put(connectionInfoCursor, "serverName", this.localIP);
		IDataUtil.put(connectionInfoCursor, "sourceIP", this.sourceIP);
		IDataUtil.put(connectionInfoCursor, "sourcePort", Integer.valueOf(this.sourcePort));
		IDataUtil.put(connectionInfoCursor, "inPayload", "inPayload");
		IDataUtil.put(connectionInfoCursor, "channel", "channel");
		IDataUtil.put(connectionInfoCursor, "backend", "backend");
		connectionInfoCursor.destroy();
		IDataUtil.put(inputCursor, "connectionInfo", connectionInfo);

		inputCursor.destroy();

		return input;
	}

	protected ISOMsg mappingDefaultDataResponse(ISOMsg isoMessage) {
		ISOMsg isoMsg = (ISOMsg) isoMessage.clone();
		try {
			isoMsg.setResponseMTI();
			isoMsg.set(39, "00");
		} catch (ISOException e) {
			e.printStackTrace();
		}
		return isoMsg;
	}

	protected ISOMsg mappingDataResponse(IData response, ISOMsg isoMessage) throws ISOException {
		IDataCursor outputCursor = response.getCursor();

		IData respMsg = IDataUtil.getIData(outputCursor, "respMsg");
		if (respMsg != null) {
			IDataCursor respMsgCursor = respMsg.getCursor();

			IData msgHeader_1 = IDataUtil.getIData(respMsgCursor, "msgHeader");
			if (msgHeader_1 != null) {
				IDataCursor msgHeader_1Cursor = msgHeader_1.getCursor();
				String msgHeaderHex_1 = IDataUtil.getString(msgHeader_1Cursor, "msgHeaderHex");
				if (msgHeaderHex_1 != null) {
					this.isoMsg.setHeader(msgHeaderHex_1.getBytes());
				}
				msgHeader_1Cursor.destroy();
			}
			IData msgDetail_1 = IDataUtil.getIData(respMsgCursor, "msgDetail");
			if (msgDetail_1 != null) {
				IDataCursor msgDetail_1Cursor = msgDetail_1.getCursor();
				String messageType = IDataUtil.getString(msgDetail_1Cursor, "messageType");
				String primaryAccountNumber = IDataUtil.getString(msgDetail_1Cursor, "primaryAccountNumber");
				String processingCode = IDataUtil.getString(msgDetail_1Cursor, "processingCode");
				String txnAmount = IDataUtil.getString(msgDetail_1Cursor, "txnAmount");
				String settlementAmount = IDataUtil.getString(msgDetail_1Cursor, "settlementAmount");
				String cardholderBillingAmount = IDataUtil.getString(msgDetail_1Cursor, "cardholderBillingAmount");
				String transmissionDatetime = IDataUtil.getString(msgDetail_1Cursor, "transmissionDatetime");
				String settlementConversionRate = IDataUtil.getString(msgDetail_1Cursor, "settlementConversionRate");
				String field9 = IDataUtil.getString(msgDetail_1Cursor, "field9");
				String cardholderBillingConversionRate = IDataUtil.getString(msgDetail_1Cursor,
						"cardholderBillingConversionRate");
				String systemTraceAuditNumber = IDataUtil.getString(msgDetail_1Cursor, "systemTraceAuditNumber");
				String localTxnTime = IDataUtil.getString(msgDetail_1Cursor, "localTxnTime");
				String localTxnDate = IDataUtil.getString(msgDetail_1Cursor, "localTxnDate");
				String expirationDate = IDataUtil.getString(msgDetail_1Cursor, "expirationDate");
				String settlementDate = IDataUtil.getString(msgDetail_1Cursor, "settlementDate");
				String conversionDate = IDataUtil.getString(msgDetail_1Cursor, "conversionDate");
				String field17 = IDataUtil.getString(msgDetail_1Cursor, "field17");
				String merchantType = IDataUtil.getString(msgDetail_1Cursor, "merchantType");
				String acquiringInstitutionCountryCode = IDataUtil.getString(msgDetail_1Cursor,
						"acquiringInstitutionCountryCode");
				String field20 = IDataUtil.getString(msgDetail_1Cursor, "field20");
				String field21 = IDataUtil.getString(msgDetail_1Cursor, "field21");
				String posEntryModeCode = IDataUtil.getString(msgDetail_1Cursor, "posEntryModeCode");
				String cardSequenceNumber = IDataUtil.getString(msgDetail_1Cursor, "cardSequenceNumber");
				String internationalNetworkID = IDataUtil.getString(msgDetail_1Cursor, "internationalNetworkID");
				String posConditionCode = IDataUtil.getString(msgDetail_1Cursor, "posConditionCode");
				String posPinCaptureCode = IDataUtil.getString(msgDetail_1Cursor, "posPinCaptureCode");
				String field27 = IDataUtil.getString(msgDetail_1Cursor, "field27");
				String txnFeeAmount = IDataUtil.getString(msgDetail_1Cursor, "txnFeeAmount");
				String field29 = IDataUtil.getString(msgDetail_1Cursor, "field29");
				String field30 = IDataUtil.getString(msgDetail_1Cursor, "field30");
				String field31 = IDataUtil.getString(msgDetail_1Cursor, "field31");
				String acquiringInstitutionIDCode = IDataUtil.getString(msgDetail_1Cursor,
						"acquiringInstitutionIDCode");
				String forwardingInstitutionIDCode = IDataUtil.getString(msgDetail_1Cursor,
						"forwardingInstitutionIDCode");
				String field34 = IDataUtil.getString(msgDetail_1Cursor, "field34");
				String track2Data = IDataUtil.getString(msgDetail_1Cursor, "track2Data");
				String track3Data = IDataUtil.getString(msgDetail_1Cursor, "track3Data");
				String retrievalReferenceNumber = IDataUtil.getString(msgDetail_1Cursor, "retrievalReferenceNumber");
				String authorizationIDResponse = IDataUtil.getString(msgDetail_1Cursor, "authorizationIDResponse");
				String responseCode = IDataUtil.getString(msgDetail_1Cursor, "responseCode");
				String field40 = IDataUtil.getString(msgDetail_1Cursor, "field40");
				String cardAcceptorTerminalID = IDataUtil.getString(msgDetail_1Cursor, "cardAcceptorTerminalID");
				String cardAcceptorIDCode = IDataUtil.getString(msgDetail_1Cursor, "cardAcceptorIDCode");
				String cardAcceptorNameLocation = IDataUtil.getString(msgDetail_1Cursor, "cardAcceptorNameLocation");
				String additionalResponseData = IDataUtil.getString(msgDetail_1Cursor, "additionalResponseData");
				String track1Data = IDataUtil.getString(msgDetail_1Cursor, "track1Data");
				String field46 = IDataUtil.getString(msgDetail_1Cursor, "field46");
				String field47 = IDataUtil.getString(msgDetail_1Cursor, "field47");
				String additionalDataPrivate48 = IDataUtil.getString(msgDetail_1Cursor, "additionalDataPrivate48");
				String txnCurrencyCode = IDataUtil.getString(msgDetail_1Cursor, "txnCurrencyCode");
				String settlementCurrencyCode = IDataUtil.getString(msgDetail_1Cursor, "settlementCurrencyCode");
				String cardholderBillingCurrencyCode = IDataUtil.getString(msgDetail_1Cursor,
						"cardholderBillingCurrencyCode");
				Object pin = IDataUtil.get(msgDetail_1Cursor, "pin");
				String securityRelatedControlInfo = IDataUtil.getString(msgDetail_1Cursor,
						"securityRelatedControlInfo");
				String additionalAmounts = IDataUtil.getString(msgDetail_1Cursor, "additionalAmounts");
				String field55 = IDataUtil.getString(msgDetail_1Cursor, "field55");
				String field56 = IDataUtil.getString(msgDetail_1Cursor, "field56");
				String additionalDataPrivate57 = IDataUtil.getString(msgDetail_1Cursor, "additionalDataPrivate57");
				String icPBOCDataReserved = IDataUtil.getString(msgDetail_1Cursor, "icPBOCDataReserved");
				String detailInquiring = IDataUtil.getString(msgDetail_1Cursor, "detailInquiring");
				String reserved60 = IDataUtil.getString(msgDetail_1Cursor, "reserved60");
				String cardholderAuthenticationInfo = IDataUtil.getString(msgDetail_1Cursor,
						"cardholderAuthenticationInfo");
				String switchingData = IDataUtil.getString(msgDetail_1Cursor, "switchingData");
				String financialNetworkData = IDataUtil.getString(msgDetail_1Cursor, "financialNetworkData");
				Object mac64 = IDataUtil.get(msgDetail_1Cursor, "mac64");
				String field65 = IDataUtil.getString(msgDetail_1Cursor, "field65");
				String settlementCode = IDataUtil.getString(msgDetail_1Cursor, "settlementCode");
				String field67 = IDataUtil.getString(msgDetail_1Cursor, "field67");
				String field68 = IDataUtil.getString(msgDetail_1Cursor, "field68");
				String field69 = IDataUtil.getString(msgDetail_1Cursor, "field69");
				String networkMgtInfoCode = IDataUtil.getString(msgDetail_1Cursor, "networkMgtInfoCode");
				String field71 = IDataUtil.getString(msgDetail_1Cursor, "field71");
				String field72 = IDataUtil.getString(msgDetail_1Cursor, "field72");
				String field73 = IDataUtil.getString(msgDetail_1Cursor, "field73");
				String creditsNumber = IDataUtil.getString(msgDetail_1Cursor, "creditsNumber");
				String creditsReversalNumber = IDataUtil.getString(msgDetail_1Cursor, "creditsReversalNumber");
				String debitsNumber = IDataUtil.getString(msgDetail_1Cursor, "debitsNumber");
				String debitsReversalNumber = IDataUtil.getString(msgDetail_1Cursor, "debitsReversalNumber");
				String transferNumber = IDataUtil.getString(msgDetail_1Cursor, "transferNumber");
				String transferReversalNumber = IDataUtil.getString(msgDetail_1Cursor, "transferReversalNumber");
				String inquiriesNumber = IDataUtil.getString(msgDetail_1Cursor, "inquiriesNumber");
				String authorizationNumber = IDataUtil.getString(msgDetail_1Cursor, "authorizationNumber");
				String creditsProcessingFee = IDataUtil.getString(msgDetail_1Cursor, "creditsProcessingFee");
				String field83 = IDataUtil.getString(msgDetail_1Cursor, "field83");
				String debitsProcessingFee = IDataUtil.getString(msgDetail_1Cursor, "debitsProcessingFee");
				String field85 = IDataUtil.getString(msgDetail_1Cursor, "field85");
				String creditsAmount = IDataUtil.getString(msgDetail_1Cursor, "creditsAmount");
				String creditsReversalAmount = IDataUtil.getString(msgDetail_1Cursor, "creditsReversalAmount");
				String debitsAmount = IDataUtil.getString(msgDetail_1Cursor, "debitsAmount");
				String debutsReversalAmount = IDataUtil.getString(msgDetail_1Cursor, "debutsReversalAmount");
				String origDataElements = IDataUtil.getString(msgDetail_1Cursor, "origDataElements");
				String field91 = IDataUtil.getString(msgDetail_1Cursor, "field91");
				String field92 = IDataUtil.getString(msgDetail_1Cursor, "field92");
				String field93 = IDataUtil.getString(msgDetail_1Cursor, "field93");
				String field94 = IDataUtil.getString(msgDetail_1Cursor, "field94");
				String replacementAmounts = IDataUtil.getString(msgDetail_1Cursor, "replacementAmounts");
				Object messageSecurityCode = IDataUtil.get(msgDetail_1Cursor, "messageSecurityCode");
				String netSettlementAmount = IDataUtil.getString(msgDetail_1Cursor, "netSettlementAmount");
				String field98 = IDataUtil.getString(msgDetail_1Cursor, "field98");
				String settlementInstitutionIDCode = IDataUtil.getString(msgDetail_1Cursor,
						"settlementInstitutionIDCode");
				String receivingInstitutionIDCode = IDataUtil.getString(msgDetail_1Cursor,
						"receivingInstitutionIDCode");
				String field101 = IDataUtil.getString(msgDetail_1Cursor, "field101");
				String accountID1 = IDataUtil.getString(msgDetail_1Cursor, "accountID1");
				String accountID2 = IDataUtil.getString(msgDetail_1Cursor, "accountID2");
				String txnDescription = IDataUtil.getString(msgDetail_1Cursor, "txnDescription");
				String field105 = IDataUtil.getString(msgDetail_1Cursor, "field105");
				String field106 = IDataUtil.getString(msgDetail_1Cursor, "field106");
				String field107 = IDataUtil.getString(msgDetail_1Cursor, "field107");
				String field108 = IDataUtil.getString(msgDetail_1Cursor, "field108");
				String field109 = IDataUtil.getString(msgDetail_1Cursor, "field109");
				String field110 = IDataUtil.getString(msgDetail_1Cursor, "field110");
				String field111 = IDataUtil.getString(msgDetail_1Cursor, "field111");
				String field112 = IDataUtil.getString(msgDetail_1Cursor, "field112");
				String field113 = IDataUtil.getString(msgDetail_1Cursor, "field113");
				String field114 = IDataUtil.getString(msgDetail_1Cursor, "field114");
				String field115 = IDataUtil.getString(msgDetail_1Cursor, "field115");
				String field116 = IDataUtil.getString(msgDetail_1Cursor, "field116");
				String field117 = IDataUtil.getString(msgDetail_1Cursor, "field117");
				String field118 = IDataUtil.getString(msgDetail_1Cursor, "field118");
				String field119 = IDataUtil.getString(msgDetail_1Cursor, "field119");
				String field120 = IDataUtil.getString(msgDetail_1Cursor, "field120");
				String reserved121 = IDataUtil.getString(msgDetail_1Cursor, "reserved121");
				String acquiringInstitutionReserved = IDataUtil.getString(msgDetail_1Cursor,
						"acquiringInstitutionReserved");
				String issuerInstitutionReserved = IDataUtil.getString(msgDetail_1Cursor, "issuerInstitutionReserved");
				String field124 = IDataUtil.getString(msgDetail_1Cursor, "field124");
				String field125 = IDataUtil.getString(msgDetail_1Cursor, "field125");
				String field126 = IDataUtil.getString(msgDetail_1Cursor, "field126");
				String field127 = IDataUtil.getString(msgDetail_1Cursor, "field127");
				Object mac128 = IDataUtil.get(msgDetail_1Cursor, "mac128");
				if (messageType != null) {
					isoMessage.setMTI(messageType);
				}
				if (primaryAccountNumber != null) {
					isoMessage.set(2, primaryAccountNumber);
				}
				if (processingCode != null) {
					isoMessage.set(3, processingCode);
				}
				if (txnAmount != null) {
					isoMessage.set(4, txnAmount);
				}
				if (settlementAmount != null) {
					isoMessage.set(5, settlementAmount);
				}
				if (cardholderBillingAmount != null) {
					isoMessage.set(6, cardholderBillingAmount);
				}
				if (transmissionDatetime != null) {
					isoMessage.set(7, transmissionDatetime);
				}
				if (settlementConversionRate != null) {
					isoMessage.set(8, settlementConversionRate);
				}
				if (field9 != null) {
					isoMessage.set(9, field9);
				}
				if (cardholderBillingConversionRate != null) {
					isoMessage.set(10, cardholderBillingConversionRate);
				}
				if (systemTraceAuditNumber != null) {
					isoMessage.set(11, systemTraceAuditNumber);
				}
				if (localTxnTime != null) {
					isoMessage.set(12, localTxnTime);
				}
				if (localTxnDate != null) {
					isoMessage.set(13, localTxnDate);
				}
				if (expirationDate != null) {
					isoMessage.set(14, expirationDate);
				}
				if (settlementDate != null) {
					isoMessage.set(15, settlementDate);
				}
				if (conversionDate != null) {
					isoMessage.set(16, conversionDate);
				}
				if (field17 != null) {
					isoMessage.set(17, field17);
				}
				if (merchantType != null) {
					isoMessage.set(18, merchantType);
				}
				if (acquiringInstitutionCountryCode != null) {
					isoMessage.set(19, acquiringInstitutionCountryCode);
				}
				if (field20 != null) {
					isoMessage.set(20, field20);
				}
				if (field21 != null) {
					isoMessage.set(21, field21);
				}
				if (posEntryModeCode != null) {
					isoMessage.set(22, posEntryModeCode);
				}
				if (cardSequenceNumber != null) {
					isoMessage.set(23, cardSequenceNumber);
				}
				if (internationalNetworkID != null) {
					isoMessage.set(24, internationalNetworkID);
				}
				if (posConditionCode != null) {
					isoMessage.set(25, posConditionCode);
				}
				if (posPinCaptureCode != null) {
					isoMessage.set(26, posPinCaptureCode);
				}
				if (field27 != null) {
					isoMessage.set(27, field27);
				}
				if (txnFeeAmount != null) {
					isoMessage.set(28, txnFeeAmount);
				}
				if (field29 != null) {
					isoMessage.set(29, field29);
				}
				if (field30 != null) {
					isoMessage.set(30, field30);
				}
				if (field31 != null) {
					isoMessage.set(31, field31);
				}
				if (acquiringInstitutionIDCode != null) {
					isoMessage.set(32, acquiringInstitutionIDCode);
				}
				if (forwardingInstitutionIDCode != null) {
					isoMessage.set(33, forwardingInstitutionIDCode);
				}
				if (field34 != null) {
					isoMessage.set(34, field34);
				}
				if (track2Data != null) {
					isoMessage.set(35, track2Data);
				}
				if (track3Data != null) {
					isoMessage.set(36, track3Data);
				}
				if (retrievalReferenceNumber != null) {
					isoMessage.set(37, retrievalReferenceNumber);
				}
				if (authorizationIDResponse != null) {
					isoMessage.set(38, authorizationIDResponse);
				}
				if (responseCode != null) {
					isoMessage.set(39, responseCode);
				}
				if (field40 != null) {
					isoMessage.set(40, field40);
				}
				if (cardAcceptorTerminalID != null) {
					isoMessage.set(41, cardAcceptorTerminalID);
				}
				if (cardAcceptorIDCode != null) {
					isoMessage.set(42, cardAcceptorIDCode);
				}
				if (cardAcceptorNameLocation != null) {
					isoMessage.set(43, cardAcceptorNameLocation);
				}
				if (additionalResponseData != null) {
					isoMessage.set(44, additionalResponseData);
				}
				if (track1Data != null) {
					isoMessage.set(45, track1Data);
				}
				if (field46 != null) {
					isoMessage.set(46, field46);
				}
				if (field47 != null) {
					isoMessage.set(47, field47);
				}
				if (additionalDataPrivate48 != null) {
					isoMessage.set(48, additionalDataPrivate48);
				}
				if (txnCurrencyCode != null) {
					isoMessage.set(49, txnCurrencyCode);
				}
				if (settlementCurrencyCode != null) {
					isoMessage.set(50, settlementCurrencyCode);
				}
				if (cardholderBillingCurrencyCode != null) {
					isoMessage.set(51, cardholderBillingCurrencyCode);
				}
				if (pin != null) {
					isoMessage.set(52, (String) pin);
				}
				if (securityRelatedControlInfo != null) {
					isoMessage.set(53, securityRelatedControlInfo);
				}
				if (additionalAmounts != null) {
					isoMessage.set(54, additionalAmounts);
				}
				if (field55 != null) {
					isoMessage.set(55, field55);
				}
				if (field56 != null) {
					isoMessage.set(56, field56);
				}
				if (additionalDataPrivate57 != null) {
					isoMessage.set(57, additionalDataPrivate57);
				}
				if (icPBOCDataReserved != null) {
					isoMessage.set(58, icPBOCDataReserved);
				}
				if (detailInquiring != null) {
					isoMessage.set(59, detailInquiring);
				}
				if (reserved60 != null) {
					isoMessage.set(60, reserved60);
				}
				if (cardholderAuthenticationInfo != null) {
					isoMessage.set(61, cardholderAuthenticationInfo);
				}
				if (switchingData != null) {
					isoMessage.set(62, switchingData);
				}
				if (financialNetworkData != null) {
					isoMessage.set(63, financialNetworkData);
				}
				if (mac64 != null) {
					isoMessage.set(64, (String) mac64);
				}
				if (field65 != null) {
					isoMessage.set(65, field65);
				}
				if (settlementCode != null) {
					isoMessage.set(66, settlementCode);
				}
				if (field67 != null) {
					isoMessage.set(67, field67);
				}
				if (field68 != null) {
					isoMessage.set(68, field68);
				}
				if (field69 != null) {
					isoMessage.set(69, field69);
				}
				if (networkMgtInfoCode != null) {
					isoMessage.set(70, networkMgtInfoCode);
				}
				if (field71 != null) {
					isoMessage.set(71, field71);
				}
				if (field72 != null) {
					isoMessage.set(72, field72);
				}
				if (field73 != null) {
					isoMessage.set(73, field73);
				}
				if (creditsNumber != null) {
					isoMessage.set(74, creditsNumber);
				}
				if (creditsReversalNumber != null) {
					isoMessage.set(75, creditsReversalNumber);
				}
				if (debitsNumber != null) {
					isoMessage.set(76, debitsNumber);
				}
				if (debitsReversalNumber != null) {
					isoMessage.set(77, debitsReversalNumber);
				}
				if (transferNumber != null) {
					isoMessage.set(78, transferNumber);
				}
				if (transferReversalNumber != null) {
					isoMessage.set(79, transferReversalNumber);
				}
				if (inquiriesNumber != null) {
					isoMessage.set(80, inquiriesNumber);
				}
				if (authorizationNumber != null) {
					isoMessage.set(81, authorizationNumber);
				}
				if (creditsProcessingFee != null) {
					isoMessage.set(82, creditsProcessingFee);
				}
				if (field83 != null) {
					isoMessage.set(83, field83);
				}
				if (debitsProcessingFee != null) {
					isoMessage.set(84, debitsProcessingFee);
				}
				if (field85 != null) {
					isoMessage.set(85, field85);
				}
				if (creditsAmount != null) {
					isoMessage.set(86, creditsAmount);
				}
				if (creditsReversalAmount != null) {
					isoMessage.set(87, creditsReversalAmount);
				}
				if (debitsAmount != null) {
					isoMessage.set(88, debitsAmount);
				}
				if (debutsReversalAmount != null) {
					isoMessage.set(89, debutsReversalAmount);
				}
				if (origDataElements != null) {
					isoMessage.set(90, origDataElements);
				}
				if (field91 != null) {
					isoMessage.set(91, field91);
				}
				if (field92 != null) {
					isoMessage.set(92, field92);
				}
				if (field93 != null) {
					isoMessage.set(93, field93);
				}
				if (field94 != null) {
					isoMessage.set(94, field94);
				}
				if (replacementAmounts != null) {
					isoMessage.set(95, replacementAmounts);
				}
				if (messageSecurityCode != null) {
					isoMessage.set(96, (String) messageSecurityCode);
				}
				if (netSettlementAmount != null) {
					isoMessage.set(97, netSettlementAmount);
				}
				if (field98 != null) {
					isoMessage.set(98, field98);
				}
				if (settlementInstitutionIDCode != null) {
					isoMessage.set(99, settlementInstitutionIDCode);
				}
				if (receivingInstitutionIDCode != null) {
					isoMessage.set(100, receivingInstitutionIDCode);
				}
				if (field101 != null) {
					isoMessage.set(101, field101);
				}
				if (accountID1 != null) {
					isoMessage.set(102, accountID1);
				}
				if (accountID2 != null) {
					isoMessage.set(103, accountID2);
				}
				if (txnDescription != null) {
					isoMessage.set(104, txnDescription);
				}
				if (field105 != null) {
					isoMessage.set(105, field105);
				}
				if (field106 != null) {
					isoMessage.set(106, field106);
				}
				if (field107 != null) {
					isoMessage.set(107, field107);
				}
				if (field108 != null) {
					isoMessage.set(108, field108);
				}
				if (field109 != null) {
					isoMessage.set(109, field109);
				}
				if (field110 != null) {
					isoMessage.set(110, field110);
				}
				if (field111 != null) {
					isoMessage.set(111, field111);
				}
				if (field112 != null) {
					isoMessage.set(112, field112);
				}
				if (field113 != null) {
					isoMessage.set(113, field113);
				}
				if (field114 != null) {
					isoMessage.set(114, field114);
				}
				if (field115 != null) {
					isoMessage.set(115, field115);
				}
				if (field116 != null) {
					isoMessage.set(116, field116);
				}
				if (field117 != null) {
					isoMessage.set(117, field117);
				}
				if (field118 != null) {
					isoMessage.set(118, field118);
				}
				if (field119 != null) {
					isoMessage.set(119, field119);
				}
				if (field120 != null) {
					isoMessage.set(120, field120);
				}
				if (reserved121 != null) {
					isoMessage.set(121, reserved121);
				}
				if (acquiringInstitutionReserved != null) {
					isoMessage.set(122, acquiringInstitutionReserved);
				}
				if (issuerInstitutionReserved != null) {
					isoMessage.set(123, issuerInstitutionReserved);
				}
				if (field124 != null) {
					isoMessage.set(124, field124);
				}
				if (field125 != null) {
					isoMessage.set(125, field125);
				}
				if (field126 != null) {
					isoMessage.set(126, field126);
				}
				if (field127 != null) {
					isoMessage.set(127, field127);
				}
				if (mac128 != null) {
					isoMessage.set(128, (String) mac128);
				}
				msgDetail_1Cursor.destroy();
			}
			respMsgCursor.destroy();
		}
		outputCursor.destroy();

		return isoMessage;
	}

	public void setBackend(String backend) {
		this.Backend = backend;
	}

	public void setDestinationIP(String destinationIP) {
		this.DestinationIP = destinationIP;
	}

	public void setDestinationPort(String destinationPort) {
		this.DestinationPort = destinationPort;
	}

	public void setTimeOut(String timeOut) {
		this.TimeOut = timeOut;
	}

	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	public void setReference3(String reference3) {
		this.reference3 = reference3;
	}

	public String getBackend() {
		return this.Backend;
	}

	public String getDestinationIP() {
		return this.DestinationIP;
	}

	public String getDestinationPort() {
		return this.DestinationPort;
	}

	public String getTimeOut() {
		return this.TimeOut;
	}

	public String getReference1() {
		return this.reference1;
	}

	public String getReference3() {
		return this.reference3;
	}
}
