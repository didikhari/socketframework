package test;

import java.io.IOException;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.iso.packager.GenericPackager;

public class TestServerListener implements ISORequestListener{

	@Override
	public boolean process(ISOSource isoSource, ISOMsg msg2) {
		try {
			System.out.println("Receive Message");
			byte[] respMsg = "1430FFF2C4018EA0E5004000000004000000165577917000000635011000000015000000000015000000000015000000090407084230000000061000000610000002948601809042102291809050904601126000170500S11009999999990215000008      294860000ATM_IDR150123456789123456\\INDOMARET\\JAKARTA\\ 000000000000IDN3603603600201002360C002710167180351200294860180904210229110099999999903471360D0300000061000000D0300000036012003600129591".getBytes();
			//byte[] respMsg = "1430FFF2C4018EA0E50040000000040000001655779170088162890110000000100000000000100000000000100000000831094211750000006100000061000000273602180831164211180901083160112600017050001100000000014021320152       273602000PRIMAATM41SIMULATOR PRIMA\\\\JAKARTA\\             IDN3603603600201002360C000829274430351200273602180831164211110000000001403471360D0075000061000000D0075000036012000003131182".getBytes();
			ISOPackager packager = new GenericPackager("/home/syb/Downloads/isoDanamonStandIn2.xml");
			ISOMsg msg = new ISOMsg();
			msg.setPackager(packager);
			msg.unpack(respMsg);
			//((ISOChannel) isoSource).send(respMsg.length respMsg);
			isoSource.send(msg);
			System.out.println("Sending response");
		} catch (ISOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
