package test;

import java.io.IOException;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMUX;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.util.NameRegistrar.NotFoundException;

public class BankChannelListener implements ISORequestListener {

	@Override
	public boolean process(ISOSource isoSource, ISOMsg isoMsg) {
		try {
			ISOMUX isomux = ISOMUX.getMUX("atmcore");
			ISOMsg respMsg = isomux.request(isoMsg, 5000l);
			if(respMsg != null)
				isoSource.send(respMsg);
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (ISOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
