package test;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.GenericPackager;

public class TestPack {

	public static void main(String[] args) throws ISOException {
		ISOPackager packager = new GenericPackager("/home/syb/Downloads/isoDanamonStandIn.xml");
		
		ISOMsg msg = new ISOMsg("0200");
		msg.setPackager(packager);
		msg.set(8, "12345678");
		msg.set(11, "12");
		byte[] pack = msg.pack();
		System.out.println(new String(pack));
		String hexDumped = ISOUtil.hexdump(pack);
		System.out.println(hexDumped);
		String str = new String(getMsgFromHexdump(hexDumped));
		System.out.println(str);
		
		/*ISOMsg msg2 = new ISOMsg();
		msg2.setPackager(packager);
		msg2.unpack("1420F332C4018CA0810040000000040000001655779170000006350110000000150000000904070842030000002948601809042102291809050904601126000170500S11009999999990215000008      294860ATM_IDR150123456789123456\\INDOMARET\\JAKARTA\\ 000000000000IDN360351200294860180904210229110099999999903471360D0300000061000000D0300000036012003600129591".getBytes());
		msg2.unpack("1430FFF2C4018EA0E5004000000004000000165577917000000635011000000015000000000015000000000015000000090407084230000000061000000610000002948601809042102291809050904601126000170500S11009999999990215000008      294860000ATM_IDR150123456789123456\\INDOMARET\\JAKARTA\\ 000000000000IDN3603603600201002360C002710167180351200294860180904210229110099999999903471360D0300000061000000D0300000036012003600129591".getBytes());
		for(int i = 0; i<=msg2.getMaxField(); i++)
		{
			System.out.println(i+" : "+msg2.getString(i));
		}*/
	}
	
	public static byte[] getMsgFromHexdump(String hexDumped) {
        StringBuffer sb = new StringBuffer();
		String[] splited = hexDumped.split("\\r?\\n");
		for (int i = 0; i < splited.length; i++) {
			sb.append(splited[i].substring(56));
		}
        byte[] b = sb.toString().getBytes();
        return b;
	}
}
