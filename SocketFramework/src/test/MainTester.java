package test;

import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMUX;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.MUX;
import org.jpos.iso.channel.NACChannel;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.q2.iso.QMUX;
import org.jpos.util.Logger;
import org.jpos.util.NameRegistrar.NotFoundException;
import org.jpos.util.ThreadPool;

import iso8583.common.admin.ISOAdministrationSocket;
import iso8583.common.channel.CustomNACChannel;
import iso8583.common.qbean.WmServiceRegistry;

public class MainTester {

	/**
	 * Test Q2 migration
	 * @param args
	 * @throws NotFoundException
	 */
	public static void main(String[] args) throws NotFoundException 
	{
		ISOAdministrationSocket.getInstance().start("deploy");
		ISOUtil.sleep(5000);
		
		MUX mux = QMUX.getMUX("dsp");
		System.out.println("mux dsp isConnected: "+mux.isConnected());
		ISOServer server = ISOServer.getServer("6666");
		System.out.println("Server: "+server.getCountersAsString());
		String wmServiceForServer = WmServiceRegistry.getWmService("6666");
		System.out.println("wmServiceForServer:"+ wmServiceForServer);
		String wmServiceForClient = WmServiceRegistry.getWmService("10.20.30.35:7777");
		System.out.println("wmServiceForClient:"+ wmServiceForClient);
		ISOChannel channel = BaseChannel.getChannel("dsp-adaptor");
		System.out.println("channel dsp:"+ channel.isConnected());
	}
	
	
	/**
	 * Test forward
	 * @param args
	 * @throws ISOException
	 */
	public static void main2(String[] args) throws ISOException 
	{
		ISOPackager packager = new GenericPackager("/home/syb/Downloads/isoDanamonStandIn.xml");
		CustomNACChannel channel = new CustomNACChannel("localhost", 7777, packager, "".getBytes());
		channel.setName("atm");
		ISOServer server = new ISOServer(7777, channel, new ThreadPool(5, 10));
		server.setName("atm");
		server.addISORequestListener(new BankChannelListener());
		//ISOTransactionCache.isoServer_obj.put("7777Server", server);

		Thread thread = new Thread(server);
		thread.start();
		
		ISOUtil.sleep(5000);

		NACChannel clientChannel = new NACChannel("localhost", 6666, packager, "".getBytes());
		Logger logger = new Logger();
		//logger.addListener(new ISOExLogListener("7777", "127.0.0.1", 0));
		clientChannel.setLogger(logger, "channel-logger");
		clientChannel.setName("atmcore");
		ISOMUX isoMux = new ISOMUX(clientChannel);
		isoMux.setName("atmcore");
		new Thread(isoMux).start();
		/*ISOUtil.sleep(2000);
		ISOMsg msg2 = new ISOMsg();
		msg2.setPackager(packager);
		msg2.unpack("1420F332C4018CA0810040000000040000001655779170000006350110000000150000000904070842030000002948601809042102291809050904601126000170500S11009999999990215000008      294860ATM_IDR150123456789123456\\INDOMARET\\JAKARTA\\ 000000000000IDN360351200294860180904210229110099999999903471360D0300000061000000D0300000036012003600129591".getBytes());
		System.out.println("Send Message");
		ISOMsg respMsg = isoMux.request(msg2, 30000l);
		System.out.println(new String(respMsg.pack()));*/
	
	}
}
