package test;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.channel.NACChannel;
import org.jpos.iso.packager.GenericPackager;
import org.jpos.util.ThreadPool;

public class ServerTest {

	public static void main(String[] args) throws ISOException 
	{
		ISOPackager packager = new GenericPackager("/home/syb/Downloads/isoDanamonStandIn2.xml");
		NACChannel channel = new NACChannel("localhost", 6666, packager, "".getBytes());
		ISOServer server = new ISOServer(6666, channel, new ThreadPool(5, 10));
		server.addISORequestListener(new TestServerListener());

		Thread thread = new Thread(server);
		thread.start();
		System.out.println("Core Simulator UP and Running");
	}
	
	public static void main1(String[] args) throws Exception {
		ServerSocket serverSocket = new ServerSocket(7777);
        System.out.println("Server siap menerima koneksi pada port ["+7777+"]");
        Socket socket = serverSocket.accept();
        InputStreamReader inStreamReader = new InputStreamReader(socket.getInputStream());
        PrintWriter sendMsg = new PrintWriter(socket.getOutputStream());
        byte[] respMsg = "1430FFF2C4018EA0E5004000000004000000165577917000000635011000000015000000000015000000000015000000090407084230000000061000000610000002948601809042102291809050904601126000170500S11009999999990215000008      294860000ATM_IDR150123456789123456\\INDOMARET\\JAKARTA\\ 000000000000IDN3603603600201002360C002710167180351200294860180904210229110099999999903471360D0300000061000000D0300000036012003600129591".getBytes();
        sendMsg.print(respMsg.length >> 8);
        sendMsg.print(respMsg.length);
        sendMsg.print(respMsg);
        sendMsg.flush();
        ISOUtil.sleep(5000);
	}
}
