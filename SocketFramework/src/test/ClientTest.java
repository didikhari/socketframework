package test;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMUX;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.GenericPackager;

import iso8583.common.channel.CustomNACChannel;

public class ClientTest {
	
	public static void main(String[] args) throws ISOException {
		ISOPackager packager = new GenericPackager("/home/syb/Downloads/isoDanamonStandIn.xml");
		CustomNACChannel channel = new CustomNACChannel("localhost", 7777, packager, "".getBytes());
		//channel.setPort(5555);
		channel.setLocalAddress("localhost", 5555);
		ISOMUX isoMux = new ISOMUX(channel);
		new Thread(isoMux).start();
		ISOUtil.sleep(2000);
		ISOMsg msg2 = new ISOMsg();
		msg2.setPackager(packager);
		//msg2.unpack("1420F332C4018CA0810040000000040000001655779170000006350110000000150000000904070842030000002948601809042102291809050904601126000170500S11009999999990215000008      294860ATM_IDR150123456789123456\\INDOMARET\\JAKARTA\\ 000000000000IDN360351200294860180904210229110099999999903471360D0300000061000000D0300000036012003600129591".getBytes());
		msg2.unpack("1420F332C4018CA081004000000004000000165577917008816289011000000010000000083109421100750000273602180831164211180901083160112600017050001100000000014021320152       273602PRIMAATM41SIMULATOR PRIMA\\\\JAKARTA\\             IDN360351200273602180831164211110000000001403471360D0075000061000000D0075000036012000003131182".getBytes());
		System.out.println("Send Message");
		ISOMsg respMsg = isoMux.request(msg2, 30000l);
		System.out.println(new String(respMsg.pack()));
	}
}
