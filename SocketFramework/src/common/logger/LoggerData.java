package common.logger;

public class LoggerData {
	public static final String eventSocketServer = "[SOCKET SERVER] ";
	public static final String eventSocketClient = "[SOCKET CLIENT] ";
	public static final String eventReceive = "[Receive]";
	public static final String eventSend = "[Send]";
	public static final String eventProcessStart = "[Starting...] ";
	public static final String eventProcessStop = "[Stopping...] ";
	public static final String eventProcessCheck = "[Checking...] ";
	public static final String eventStart = "[Start] ";
	public static final String eventFinish = "[Finish] ";
}
