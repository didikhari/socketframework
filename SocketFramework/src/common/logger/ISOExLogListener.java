/*package common.logger;

import java.io.IOException;
import java.util.List;

import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOServer;
import org.jpos.util.LogEvent;
import org.jpos.util.LogListener;
import org.jpos.util.NameRegistrar.NotFoundException;

import iso8583.common.cache.ISOTransactionCache;
import iso8583.common.channel.CustomNACChannel;

public class ISOExLogListener implements LogListener {

	private static Log logger = Log.getInstance(ISOExLogListener.class);
	
	private String sourcePort;
	private String fwIp;
	private Integer fwPort;
	public ISOExLogListener(String sourcePort) {
		this.sourcePort = sourcePort;
	}
	
	public ISOExLogListener(String sourcePort, String ip, int port) {
		this.sourcePort = sourcePort;
		this.fwIp = ip;
		this.fwPort = port;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public LogEvent log(LogEvent evt) 
	{
		String header = "";
		String message = "";
		if("receive".equals(evt.getTag())) 
		{
			List payLoad = evt.getPayLoad();
			for (int i = 0; i < payLoad.size(); i++) 
			{
				if(String.valueOf(payLoad.get(i)).contains("--- header ---")) 
				{
					header = getASCIIFromISOHexdump(String.valueOf(payLoad.get(i+1)));
				}
				else if(String.valueOf(payLoad.get(i)).contains("--- data ---"))
				{
					message = getASCIIFromISOHexdump(String.valueOf(payLoad.get(i+1)));
				}
			}
			
			if(message != null && message.trim().length() > 0)
			{
				logger.info("Failed message parsing detected");
				logger.info("Header: "+header);
				logger.info("Message: "+message);
				try 
				{
					logger.info("Getting ISOServer "+sourcePort + "Server");
					ISOServer isoServer = (ISOServer) ISOTransactionCache.isoServer_obj.get(sourcePort + "Server");
					if(isoServer == null)
						throw new NotFoundException("ISO Server for "+sourcePort + "Server Not Found!");
					String countersAsString = isoServer.getCountersAsString();
					logger.info("ISOServer: "+sourcePort + "Server Counter: "+countersAsString);
					//ISOServer isoServer = ISOServer.getServer(serverName);
					if((fwIp != null && fwIp.trim().length() > 0) && (fwPort != null && fwPort != 0))
					{
						ISOChannel bankChannel = isoServer.getISOChannel(fwIp+":"+fwPort);
						if(bankChannel == null) {
							logger.error("Connection to "+fwIp+":"+fwPort +"Not Found");
							logger.info("ISOServer: "+sourcePort + "Server Available Connections: "+isoServer.getISOChannelNames());
						} else {
							CustomNACChannel isoChannel = (CustomNACChannel) bankChannel;
							logger.info("Forward message to: "+isoChannel.getName());
							isoChannel.send(message.getBytes());
							logger.info("Message forwarded");
						}
					}
					else
					{
						if(fwIp != null && fwIp.trim().length() > 0)
						{
							String isoChannelNames = isoServer.getISOChannelNames();
							if(isoChannelNames != null && isoChannelNames.trim().length() > 0)
							{
								String[] splitedConnectionName = isoChannelNames.split(" ");
								String connectedChannelName = null;
								for (int i = 0; i < splitedConnectionName.length; i++) 
								{
									if(splitedConnectionName[i].startsWith(fwIp))
									{
										connectedChannelName = splitedConnectionName[i];
										break;
									}
								}
								
								if(connectedChannelName != null)
								{
									ISOChannel connectedChannel = isoServer.getISOChannel(connectedChannelName);
									//CustomNACChannel isoChannel = (CustomNACChannel) connectedChannel;
									logger.info("Forward message to: "+connectedChannel.getName());
									connectedChannel.send(message.getBytes());
									logger.info("Message forwarded");
								}
								else
								{
									logger.error("Channel with ip "+this.fwIp +" is Not connected");
								}
							}
							else
							{
								logger.error("No channel connected to server");
							}
						}
						else
						{
							logger.error("Channel IP Addess is not set");
						}
						
					}
					
				} catch (NotFoundException e) {
					logger.error("ISOServer not found", e);
				} catch (IOException e) {
					logger.error("IOException", e);
				} catch (ISOException e) {
					logger.error("ISOException", e);
				} catch (Exception e) {
					logger.error("Exception", e);
				}
			}
			
		}
		return evt;
	}
	
	private String getASCIIFromISOHexdump(String hexDumped) {
        StringBuffer sb = new StringBuffer();
		String[] splited = hexDumped.split("\\r?\\n");
		for (int i = 0; i < splited.length; i++) {
			sb.append(splited[i].substring(56));
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		String names = "10.20.30.401:2484 10.20.49.382:9102";
		String[] split = names.split(" ");
		for (String string : split) {
			System.out.println(string);
		}
	}
}
*/