package common.logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Log {

	private Logger m_logger = null;
	private static String m_loggerName = "socket_framework";
	private static String m_mutex = "mutex";
	private static boolean m_isInitialized = false;

	public static Log getInstance() {
		return new Log();
	}

	public static Log getInstance(Class paramClass) {
		return new Log(m_loggerName);
	}

	public static Log getInstance(String paramString) {
		return new Log(paramString);
	}

	public static void init(Properties paramProperties) {
		try {
			PropertyConfigurator.configure(paramProperties);
		} catch (Exception localException) {
			initLog4J();
		}
		synchronized (m_mutex) {
			m_isInitialized = true;
		}
	}

	public static void init(String paramString) {
		System.out.println("Init log4j " + paramString);
		try {
			FileInputStream localFileInputStream = new FileInputStream(paramString);
			Properties localProperties = new Properties();
			localProperties.load(localFileInputStream);
			PropertyConfigurator.configure(localProperties);
			if (localFileInputStream != null) {
				try {
					localFileInputStream.close();
				} catch (Exception localException2) {
				}
			}
		} catch (Exception localException1) {
			System.out.println("Errorrr");
			initLog4J();
			localException1.printStackTrace();
		}
		synchronized (m_mutex) {
			m_isInitialized = true;
		}
	}

	public boolean isDebugEnabled() {
		return this.m_logger.isDebugEnabled();
	}

	public boolean isInfoEnabled() {
		return this.m_logger.isInfoEnabled();
	}

	public boolean isWarnEnabled() {
		return this.m_logger.isEnabledFor(Level.WARN);
	}

	public boolean isErrorEnabled() {
		return this.m_logger.isEnabledFor(Level.ERROR);
	}

	public boolean isFatalEnabled() {
		return this.m_logger.isEnabledFor(Level.FATAL);
	}

	public void debug(Object paramObject) {
		try {
			if (this.m_logger.isDebugEnabled()) {
				this.m_logger.debug(paramObject);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void debug(Object paramObject, Throwable paramThrowable) {
		try {
			if (this.m_logger.isDebugEnabled()) {
				this.m_logger.debug(paramObject, paramThrowable);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void info(Object paramObject) {
		try {
			if (this.m_logger.isInfoEnabled()) {
				this.m_logger.info(paramObject);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void info(Object paramObject, Throwable paramThrowable) {
		try {
			if (this.m_logger.isInfoEnabled()) {
				this.m_logger.info(paramObject, paramThrowable);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void warn(Object paramObject) {
		try {
			if (this.m_logger.isEnabledFor(Level.WARN)) {
				this.m_logger.warn(paramObject);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void warn(Object paramObject, Throwable paramThrowable) {
		try {
			if (this.m_logger.isEnabledFor(Level.WARN)) {
				this.m_logger.warn(paramObject, paramThrowable);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void error(Object paramObject) {
		try {
			if (this.m_logger.isEnabledFor(Level.ERROR)) {
				this.m_logger.error(paramObject);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void error(Object paramObject, Throwable paramThrowable) {
		try {
			if (this.m_logger.isEnabledFor(Level.ERROR)) {
				this.m_logger.error(paramObject, paramThrowable);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void fatal(Object paramObject) {
		try {
			if (this.m_logger.isEnabledFor(Level.FATAL)) {
				this.m_logger.fatal(paramObject);
			}
		} catch (Throwable localThrowable) {
		}
	}

	public void fatal(Object paramObject, Throwable paramThrowable) {
		try {
			if (this.m_logger.isEnabledFor(Level.FATAL)) {
				this.m_logger.fatal(paramObject, paramThrowable);
			}
		} catch (Throwable localThrowable) {
		}
	}

	private static void init() {
		if (!m_isInitialized) {
			initLog4J();
			synchronized (m_mutex) {
				m_isInitialized = true;
			}
		}
	}

	private static void initLog4J() {
		try {
			InputStream localInputStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("log4j.properties");
			Properties localProperties = new Properties();
			localProperties.load(localInputStream);
			PropertyConfigurator.configure(localProperties);
		} catch (Exception localException) {
			BasicConfigurator.configure();
		}
	}

	private Log() {
		init();
		this.m_logger = Logger.getRootLogger();
	}

	private Log(Class paramClass) {
		init();
		this.m_logger = Logger.getLogger(paramClass);
	}

	private Log(String paramString) {
		init();
		this.m_logger = Logger.getLogger(paramString);
	}
}
