package common.logger;

public class LoggerConfig {
	public void configLog(String propertiesFile) {
		configEAILogger(propertiesFile);
	}

	protected void configEAILogger(String propertiesFile) {
		LoggerFactory loggerFactory = new LoggerFactory();
		loggerFactory.eaiLogger(propertiesFile);
	}
}
